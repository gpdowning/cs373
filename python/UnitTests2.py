#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------------
# UnitTests2.py
# -------------

# https://docs.python.org/3/library/unittest.html
# https://docs.python.org/3/library/unittest.html#assert-methods

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test1 (self) :
        self.assertEqual(cycle_length( 1), 1)

    def test2 (self) :
        self.assertEqual(cycle_length( 5), 5)

    def test3 (self) :
        self.assertEqual(cycle_length(10), 7)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
# -v: verbose
% ./UnitTests2.py -v
test1 (__main__.MyUnitTests.test1) ... ok
test2 (__main__.MyUnitTests.test2) ... FAIL
test3 (__main__.MyUnitTests.test3) ... ok

======================================================================
FAIL: test2 (__main__.MyUnitTests.test2)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/./UnitTests2.py", line 34, in test2
    self.assertEqual(cycle_length( 5), 5)
    ~~~~~~~~~~~~~~~~^^^^^^^^^^^^^^^^^^^^^
AssertionError: 6 != 5

----------------------------------------------------------------------
Ran 3 tests in 0.001s

FAILED (failures=1)
"""
