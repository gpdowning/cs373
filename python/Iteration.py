#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ------------
# Iteration.py
# ------------

# https://docs.python.org/3.9/tutorial/controlflow.html
# https://docs.python.org/3.9/library/itertools.html

import collections
import itertools

def test1 () -> None :
    a: list[int] = [2, 3, 4]
    assert isinstance(a, list)
    assert not hasattr(a, "__next__")
    assert     hasattr(a, "__iter__")
    assert     hasattr(a, "__getitem__")
    assert     hasattr(a, "__len__")
    s: int = 0
    v: int
    for v in a :
        s += v
    assert s == 9

def test2 () -> None :
    a: list[int] = [2, 3, 4]
    v: int
    for v in a :
        v += 1            # ?
    assert a == [2, 3, 4]

def test3 () -> None :
    a: list[list[int]] = [[2], [3], [4]]
    v: list[int]
    for v in a :
        v += (5,)                        # ?
    assert a == [[2, 5], [3, 5], [4, 5]]

def test4 () -> None :
    a: list[tuple[int, ...]] = [(2,), (3,), (4,)]
    v: tuple[int, ...]
    for v in a :
        v += (5,)                  # ?
    assert a == [(2,), (3,), (4,)]

def test5 () -> None :
    a: list[str] = ["abc", "def", "ghi"]
    v: str
    for v in a :
        v += "x"                      # ?
    assert a == ["abc", "def", "ghi"]

def test6 () -> None :
    a: list = [[2, "abc"], (3, "def"), [4, "ghi"]]
    s: int  = 0
    u: int
    for u, _ in a :
        s += u
    assert s == 9

def test7 () -> None :
    x: set[int] = {2, 3, 4}
    assert isinstance(x, set)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert not hasattr(x, "__getitem__")
    assert     hasattr(x, "__len__")
    s: int = 0
    v: int
    for v in x :                         # order IS NOT guaranteed
        s += v
    assert s == 9

def test8 () -> None :
    d: dict[int, str] = {2: "abc", 3: "def", 4: "ghi"}
    assert isinstance(d, dict)
    assert not hasattr(d, "__next__")
    assert     hasattr(d, "__iter__")
    assert     hasattr(d, "__getitem__")
    assert     hasattr(d, "__len__")
    s: int = 0
    k: int
    for k in d :                         # order IS guaranteed
        s += k
    assert s == 9

def test9 () -> None :
    d: dict[int, str]            = {2: "abc", 3: "def"}
    k1: collections.abc.KeysView = d.keys()
    assert str(type(k1)) == "<class 'dict_keys'>"
    assert not hasattr(k1, "__next__")
    assert     hasattr(k1, "__iter__")
    assert not hasattr(k1, "__getitem__")
    assert     hasattr(k1, "__len__")
    assert list(k1) == [2, 3]
    k2: collections.abc.KeysView = d.keys()
    assert k1 is not k2
    assert k1 ==     k2
    d[4] = "ghi"
    assert d == {2: "abc", 3: "def", 4: "ghi"}
    assert list(k1) == [2, 3, 4]
    assert list(k2) == [2, 3, 4]

def test10 () -> None :
    d: dict[int, str]             = {2: "abc", 3: "def", 4: "ghi"}
    v: collections.abc.ValuesView = d.values()
    assert str(type(v)) == "<class 'dict_values'>"
    assert list(v)      == ["abc", "def", "ghi"]

def test11 () -> None :
    d:  dict[int, str]            = {2: "abc", 3: "def", 4: "ghi"}
    kv: collections.abc.ItemsView = d.items()
    assert str(type(kv)) == "<class 'dict_items'>"
    assert list(kv)      == [(2, "abc"), (3, "def"), (4, "ghi")]

def test12 () -> None :
    r: range = range(10)
    assert isinstance(r, range)
    assert not hasattr(r, "__next__")
    assert     hasattr(r, "__iter__")
    assert     hasattr(r, "__getitem__")
    assert     hasattr(r, "__len__")
    assert list(r) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert list(r) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    s: range = range(10)
    assert r is not s
    assert r ==     s

def test13 () -> None :
    r: range = range(2, 10)
    assert list(r) == [2, 3, 4, 5, 6, 7, 8, 9]

def test14 () -> None :
    r: range = range(2, 10, 2)
    assert list(r) == [2, 4, 6, 8]

def test15 () -> None :
    r: range = range(10, 2, -2)
    assert list(r) == [10, 8, 6, 4]

def test16 () -> None :
    r: range = range(10)
    assert r[0] == 0
    assert r[9] == 9
    try :
        assert r[10] == 10 # error: out of range
        assert False
    except IndexError :
        pass
    #r[0] = 2              # TypeError: 'range' object does not support item assignment

def test17 () -> None :
    r: range = range(15)
    s: int   = 0
    v: int
    for v in r :
        if v == 10 :
            break
        s += v
    else :           # else clause in a for loop
        assert False # executes when the loop terminates normally
    assert s == 45

def test18 () -> None :
    c: itertools.count = itertools.count() # 0, 1, 2, ...
    assert isinstance(c, itertools.count)
    assert     hasattr(c, "__next__")
    assert     hasattr(c, "__iter__")
    assert not hasattr(c, "__getitem__")
    assert not hasattr(c, "__len__")
    assert iter(c) is c
    s: int = 0
    v: int
    for v in c :
        if v > 4 :
            break
        s += v
    assert s == 10                         # 0+1+2+3+4 = 10
    s = 0
    for v in c :
        if v > 9 :
            break
        s += v
    assert s == 30                         # 6+7+8+9 = 30

def test19 () -> None :
    c: itertools.count = itertools.count(3, 2) # 3, 5, 7, 9, ...
    s: int = 0
    v: int
    for v in c :
        if v > 7 :
            break
        s += v                                 # 3+5+7 = 15
    assert s == 15

def test20 () -> None :
    c: itertools.count = itertools.count(3, -2) # 3, 1, -1, -3, ...
    s: int = 0
    v: int
    for v in c :
        if v < -1 :
            break
        s += v
    assert s == 3                               # 3+1-1 = 3

def test21 () -> None :
    z: zip = zip([2, 3], (4, 5, 6))
    assert isinstance(z, zip)
    assert     hasattr(z, "__next__")
    assert     hasattr(z, "__iter__")
    assert not hasattr(z, "__getitem__")
    assert not hasattr(z, "__len__")
    assert iter(z) is z
    assert list(z) == [(2, 4), (3, 5)]
    assert not list(z)

def test22 () -> None :
    z: zip = zip([2, 3], itertools.count())
    assert list(z) == [(2, 0), (3, 1)]
    assert not list(z)

def main () :
    print("Iteration.py")
    for i in range(22) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
