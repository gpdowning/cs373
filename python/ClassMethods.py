#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# ---------------
# ClassMethods.py
# ---------------

class A :
    cv = 0

    def __init__ (self) :
        A.cv    += 1
        self.iv  = 1

    def im (self) :
        pass

    @classmethod
    def cm (cls) :  # cls might be bound to a child type
        # cv   += 1 # UnboundLocalError: cannot access local variable 'cv' where it is not associated with a value
        cls.cv += 1

        # im()      # NameError: name 'im' is not defined
        # cls.im()  # TypeError: im() missing 1 required positional argument: 'self'

        # sm()      # NameError: name 'sm' is not defined
        cls.sm()

    @staticmethod
    def sm () :
        pass

def test1 () :
    assert A.cv == 0

    A.cm()
    assert A.cv == 1

def test2 () :
    x = A()
    assert A.cv == 2
    assert x.iv == 1

    A.cm()
    x.cm()           # misleading
    assert A.cv == 4
    assert x.iv == 1

class MyList :
    def __init__ (self, a) :
        self.a = a

    @classmethod
    def from_size (cls, s) :
        return cls(s * [0])

    @classmethod
    def from_size_value (cls, s, v) :
        return cls(s * [v])

class MyListChild (MyList) :
    pass

def test3 () :
    x = MyList.from_size(2)
    assert isinstance(x, MyList)
    assert x.a == [0, 0]

def test4 () :
    x = MyList.from_size_value(2, 3)
    assert isinstance(x, MyList)
    assert x.a == [3, 3]

def test5 () :
    x = MyListChild.from_size(2)
    assert isinstance(x, MyListChild)
    assert x.a == [0, 0]

def test6 () :
    x = MyListChild.from_size_value(2, 3)
    assert isinstance(x, MyListChild)
    assert x.a == [3, 3]

def main () :
    print("ClassMethods.py")
    for n in range(6) :
        eval("test" + str(n + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
