#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ----------
# Reduce1.py
# ----------

# https://docs.python.org/3.6/library/functools.html

import operator

"""
v <bf> a[0] <bf> a[1] <bf> a[2] <bf> ... <bf> a[n-1]
"""

def my_reduce (bf, xs, v) :
    for x in xs :
        v = bf(v, x)
    return v

def my_reduce_recursive (bf, xs, v) :
    if not xs :
        return v
    *xs, y = xs
    return bf(my_reduce(bf, xs, v), y)

def test1 () :
    # 0 + 2 + 3 + 4
    # add(add(add(0, 2), 3), 4)
    assert my_reduce(operator.add, (2, 3, 4),          0)  ==  9

def test2 () :
    # 1 * 2 * 3 * 4
    # mul(mul(mul(1, 2), 3), 4)
    assert my_reduce(operator.mul, {2, 3, 4},          1)  == 24

def test3 () :
    # 0 - 2 - 3 - 4
    # sub(sub(sub(0, 2), 3), 4)
    assert my_reduce(operator.sub, [2, 3, 4],          0)  == -9

def test4 () :
    # [] <bf> 2 <bf> 3 <bf> 4
    assert my_reduce(lambda v, x : [*v, x], [2, 3, 4], []) == [2, 3, 4]

def test5 () :
    assert my_reduce(None,         [],                 3)  ==  3

def main () :
    print("Reduce.py")
    test1()
    test2()
    test3()
    test4()
    test5()
    """
    n = int(input())
    eval("test" + str(n) + "()")
    """
    print("Done.")

if __name__ == "__main__" : #pragma: no cover
    main()
