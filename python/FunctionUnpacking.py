#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# --------------------
# FunctionUnpacking.py
# --------------------

def f (x, y, z) :
    return [x, y, z]

def test1 () :
    t = (3, 4)
    assert f(2, 5, t)  == [2, 5, (3, 4)]
    assert f(2,   *t)  == [2, 3, 4]      # order matters
    assert f(*t,  2)   == [3, 4, 2]      # position and iterable unpacking are processed left to right
    assert f(z=2, *t)  == [3, 4, 2]      # unpacking goes first, order doesn't matter
    assert f(*t,  z=2) == [3, 4, 2]
    # f(x=2, *t)                         # TypeError: f() got multiple values for argument 'x'
    # f(*t,  x=2)                        # TypeError: f() got multiple values for argument 'x'

def test2 () :
    u = (2, 3)
    v = (4,)
    assert f(*u, *v)     == [2, 3, 4]
    assert  [*u, *v, *u] == [2, 3, 4, 2, 3]
    assert  (*u, *v, *u) == (2, 3, 4, 2, 3)
    assert  {*u, *v, *u} == {2, 3, 4}

def test3 () :
    a      = [2, 3, 4]
    x, *xs = a
    assert x  == 2
    assert isinstance(xs, list)
    assert xs == [3, 4]
    x, y, *xs = a
    assert x  == 2
    assert y  == 3
    assert xs == [4]
    *xs, x = a
    assert xs == [2, 3]
    assert x  == 4

class MyList :
    def __init__ (self, a) :
        self.a = a

    def __iter__ (self) :
        return iter(self.a)

def test4 () :
    a = [2, 3, 4]
    x = MyList(a)
    assert [*x, *x] == [2, 3, 4, 2, 3, 4]

def test5 () :
    d = {"z" : 4, "y" : 3, "x" : 2}
    assert f(**d) == [2, 3, 4]

def test6 () :
    d = {"z" : 4, "y" : 3}
    assert f(2,   **d) == [2, 3, 4]
    # f(**d, 2)                       # SyntaxError: invalid syntax; position must precede dict unpacking
    assert f(x=2, **d) == [2, 3, 4]   # order doesn't matter
    assert f(**d, x=2) == [2, 3, 4]
    # assert f(z=2, **d) == [2, 3, 4] # TypeError: f() got multiple values for keyword argument 'z'
    # assert f(**d, z=2) == [2, 3, 4] # TypeError: f() got multiple values for keyword argument 'z'

def test7 () :
    t = (2, 3)
    d = {"z" : 4}
    assert f(*t, **d) == [2, 3, 4]
    # f(**d, *t)                      # SyntaxError: iterable argument unpacking follows keyword argument unpacking; iterable unpacking must precede dict unpacking

def test8 () :
    u = {"x":2, "y":3}
    v = {"z":4}
    assert f(**u, **v)       == [2, 3, 4]
    assert  {**u, **v, **u}  == {"x": 2, "y": 3, "z": 4}

class MyDict :
    def __init__ (self, d) :
        self.d = d

    def __getitem__ (self, k) :
        return self.d[k]

    def keys (self) :
        return self.d.keys()

def test9 () :
    d = {"z" : 4, "y" : 3, "x" : 2}
    x = MyDict(d)
    assert {**x, **x} == {"x" : 2, "y" : 3, "z" : 4}

def main () :
    print("FunctionUnpacking.py")
    for i in range(9) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
