#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -----------
# Reduce2T.py
# -----------

# https://docs.python.org/3.6/library/functools.html

import functools
import operator
import typing
import unittest

"""
v <bf> a[0] <bf> a[1] <bf> a[2] <bf> ... <bf> a[n-1]
or
a[0] <bf> a[1] <bf> a[2] <bf> ... <bf> a[n-1]
"""

T = typing.TypeVar("T")

def my_reduce (
        bf: typing.Callable[[T, T], T],
        xs: typing.Iterable[T],
        v:  typing.Optional[T] = None) \
        -> T :
    if not xs and v is None :
        raise TypeError
    if v is None :
        v, *xs = xs
    for x in xs :
        v = bf(v, x)
    return v

class MyUnitTests (unittest.TestCase) :
    def setUp (self) :
        self.a = [
            my_reduce,
            functools.reduce]

    def test1 (self) :
        # 0 + 2 + 3 + 4
        # add(add(add(0, 2), 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.add, (2, 3, 4), 0), 9)

    def test2 (self) :
        # 1 * 2 * 3 * 4
        # mul(mul(mul(1, 2), 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.mul, {2, 3, 4}, 1), 24)

    def test3 (self) :
        # 0 - 2 - 3 - 4
        # sub(sub(sub(0, 2), 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.sub, [2, 3, 4], 0), -9)

    def test4 (self) :
        # [] <bf> 2 <bf> 3 <bf> 4
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(lambda v, x : [*v, x], [2, 3, 4], []), [2, 3, 4])

    def test5 (self) :
        # 2 + 3 + 4
        # add(add(2, 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.add, (2, 3, 4)), 9)

    def test6 (self) :
        # 2 * 3 * 4
        # mul(mul(2, 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.mul, (2, 3, 4)), 24)

    def test7 (self) :
        # 2 - 3 - 4
        # sub(sub(2, 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.sub, (2, 3, 4)), -5)

    def test8 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                try :
                    f(None, [])
                    assert False
                except TypeError :
                    pass

if __name__ == "__main__" : #pragma: no cover
    unittest.main()
