#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------------
# Assignment.py
# -------------

def test1 () :
    x = y = 2
    assert x  == 2
    assert y  == 2

def test2 () :
    x, y = 2, 3
    assert x  == 2
    assert y  == 3
    x, y = y, x
    assert x  == 3
    assert y  == 2

def test3 () :
    x, y = [2, 3]
    assert x  == 2
    assert y  == 3

def test4 () :
    x = y, z = [2, 3]
    assert x  == [2, 3]
    assert y  == 2
    assert z  == 3
    x, y = z = [2, 3]
    assert x  == 2
    assert y  == 3
    assert z  == [2, 3]

def test5 () :
    x, *ys = {2}
    assert x  == 2
    assert not ys
    x, *ys = [2]
    assert x  == 2
    assert not ys
    x, *ys = [2, 3]
    assert x  == 2
    assert ys == [3]
    x, *ys = [2, 3, 4]
    assert x  == 2
    assert ys == [3, 4]

def test6 () :
    *xs, y = {2}
    assert not xs
    assert y  == 2
    *xs, y = [2]
    assert not xs
    assert y  == 2
    *xs, y = [2, 3]
    assert xs == [2]
    assert y  == 3
    *xs, y = [2, 3, 4]
    assert xs == [2, 3]
    assert y  == 4

def test7 () :
    x, *ys, z = [2, 3]
    assert x  == 2
    assert not ys
    assert z  == 3
    x, *ys, z = [2, 3, 4]
    assert x  == 2
    assert ys == [3]
    assert z  == 4

def main () :
    print("Assignment.py")
    for i in range(7) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
