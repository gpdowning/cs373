#!/usr/bin/env python3

# pylint: disable = duplicate-key
# pylint: disable = duplicate-value
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = literal-comparison
# pylint: disable = missing-docstring
# pylint: disable = no-member
# pylint: disable = too-few-public-methods
# pylint: disable = use-dict-literal
# pylint: disable = use-list-literal

# --------
# Types.py
# --------

# https://docs.python.org/3.6/library/types.html

from typing import Any, Callable

import collections
import types

def test1 () -> None :
    b: bool = bool()              # bools are stored by value
    c: bool = False
    assert b is c
    assert isinstance(b,    bool)
    assert isinstance(bool, type)

def test2 () -> None :
    i: int = int()               # ints are store by value here
    j: int = 0
    assert i is j
    i  = 2
    i += 1
    j  = 3
    assert i is j
    i  = 255
    i +=   1
    j  = 256
    assert i is j
    i  = 256
    i +=   1
    j  = 257
    assert i is not j            # ints are stored by address here
    assert i ==     j            # cache: [-5, 256]
    k: int = 257
    assert j is     k            # literal caching
    i =  -4
    i -=  1
    j =  -5
    assert i is j
    i =  -5
    i -=  1
    j =  -6
    assert i is not j            # ints are stored by address here
    assert i ==     j            # cache: [-5, 256]
    assert isinstance(i,   int)
    assert isinstance(int, type)

def test3 () -> None :
    f: float = float()
    g: float = 0.0
    assert f is not g               # floats are stored by address
    assert f ==     g
    h: float = 0.0
    assert g is     h               # literal caching
    assert isinstance(f,     float)
    assert isinstance(float, type)

def test4 () -> None :
    c: complex = complex()
    d: complex = 0 + 0j
    assert c is not d                   # complexes are stored by address
    assert c ==     d
    e: complex = 0 + 0j
    assert d is     e                   # literal caching
    assert isinstance(c,       complex)
    assert isinstance(complex, type)

def test5 () -> None :
    s: str = str()               # str is like Java's String
    t: str = ""
    assert s is t                # strs are stored by address, but empty is special
    s = "ab"
    s += "c"
    t = "abc"
    assert s is not t
    assert s ==     t
    u: str = "abc"
    assert t is u                # literal caching
    assert isinstance(s,   str)
    assert isinstance(str, type)

def test6 () -> None :
    a: list = list()              # list is like Java's ArrayList
    b: list = []
    assert a is not b             # lists are stored by address
    assert a ==     b
    a = [2, "abc", 3.45]          # lists are heterogeneous
    b = [2, "abc", 3.45]
    assert a is not b             # no literal caching
    assert a ==     b
    assert a == [2, "abc", 3.45]
    assert a != ["abc", 2, 3.45]  # order matters
    assert isinstance(a,    list)
    assert isinstance(list, type)

def test7 () -> None :
    u: tuple = tuple()              # tuples are like lists, but immutable in size and value
    v: tuple = ()
    assert u is v                   # tuples are stored by address, but empty is special
    u = tuple([2, "abc", 3.45])
    v = (2, "abc", 3.45)
    assert u is not v
    w: tuple = (2, "abc", 3.45)
    assert v is w                   # literal caching
    assert u == (2, "abc", 3.45)
    assert u != ("abc", 2, 3.45)    # order matters
    assert isinstance(u,     tuple)
    assert isinstance(tuple, type)

def test8 () -> None :
    x: set = set()                  # sets are like Java's HashSet
    y: set = set()                  # sets have immutable values
    assert x is not y               # sets are stored by address
    assert x ==     y
    x = {2, "abc", 3.45}
    y = {2, "abc", 3.45}
    assert x is not y               # no literal caching
    assert x ==     y
    x = {2, 2, "abc", 3.45}
    assert x == {2, "abc", 3.45}    # sets have unique values
    assert x == {"abc", 2, 3.45}    # order does not matter
    assert isinstance(x,   set)
    assert isinstance(set, type)

def test9 () -> None :
    x: frozenset = frozenset()              # frozensets are like sets, but immutable in size
    y: frozenset = frozenset()
    assert x is not y                       # frozensets are stored by address
    assert x ==     y
    x = frozenset((2, 2, "abc", 3.45))
    assert x == frozenset([2, "abc", 3.45]) # frozensets have unique values
    assert x == frozenset(["abc", 2, 3.45]) # order does not matter
    assert isinstance(y,         frozenset)
    assert isinstance(frozenset, type)

def test10 () -> None :
    d: dict = dict()                             # dicts are like Java's HashMap
    e: dict = {}                                 # dicts have immutable keys
    assert d is not e                            # dicts are stored by address
    assert d ==     e
    d = {2: "abc", 3: "def", 4: "ghi"}
    e = {2: "abc", 3: "def", 4: "ghi"}
    assert d is not e                            # no literal caching
    assert d ==     e
    d = {2: "xxx", 2: "abc", 3: "def", 4: "ghi"}
    assert d == {2: "abc", 3: "def", 4: "ghi"}   # dicts have unique keys
    assert d == {3: "def", 2: "abc", 4: "ghi"}   # order does not matter
    assert isinstance(d,    dict)
    assert isinstance(dict, type)

def test11 () -> None :
    q: collections.deque = collections.deque()              # deques are like Java's LinkedList
    r: collections.deque = collections.deque()
    assert q is not r                                       # deques are stored by address
    assert q ==     r
    q = collections.deque((2, "abc", 3.45))
    assert q == collections.deque((2, "abc", 3.45))
    assert q != collections.deque(("abc", 2, 3.45))         # order matters
    assert isinstance(q,                 collections.deque)
    assert isinstance(collections.deque, type)

def test12 () -> None :
    def f (v) :
        return v + 1
    assert isinstance(f,                  types.FunctionType)
    assert isinstance(types.FunctionType, type)

def test13 () -> None :
    f: Callable[[Any], Any] = lambda v : v + 1
    assert isinstance(f,                  types.FunctionType)
    assert isinstance(types.FunctionType, type)

def test14 () -> None :
    class A :
        def __init__ (self, i, s, f) :
            self.i = i
            self.s = s
            self.f = f

    x: A = A(2, "abc", 3.45)
    y: A = A(2, "abc", 3.45)
    assert x is not y               # objects are stored by addresss
    assert x !=     y
    assert isinstance(x,    A)
    assert isinstance(x,    object)
    assert isinstance(A,    type)
    assert isinstance(type, type)

    assert issubclass(A, A)
    assert issubclass(A, object)

    assert issubclass(type, type)
    assert issubclass(type, object)

    assert issubclass(object, object)

def test15 () -> None :
    class A :
        def __init__ (self, i, s, f) :
            self.i = i
            self.s = s
            self.f = f

        def __eq__ (self, rhs) :
            return (self.i == rhs.i) and (self.s == rhs.s) and (self.f == rhs.f)

    x: A = A(2, "abc", 3.45)
    y: A = A(2, "abc", 3.45)
    assert x is not y
    assert x ==     y

def main () -> None :
    print("Types.py")
    for i in range(15) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
