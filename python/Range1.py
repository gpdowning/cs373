#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement
# pylint: disable = too-few-public-methods

# ---------
# Range1.py
# ---------

# https://docs.python.org/3/library/functions.html#func-range

class my_range () :
    class iterator () :
        def __init__ (self, start, stop) :
            self.start = start
            self.stop  = stop

        def __iter__ (self) :
            return self

        def __next__ (self) :
            if self.start >= self.stop :
                raise StopIteration()
            start       = self.start
            self.start += 1
            return start

    def __init__ (self, start, stop) :
        self.start = start
        self.stop  = stop

    def __eq__ (self, rhs) :
        return (self.start == rhs.start) and (self.stop == rhs.stop)

    def __getitem__ (self, index) :
        if 0 <= index < (self.stop - self.start) :
            return self.start + index
        if 0 > index >= (self.start - self.stop) :
            return self.stop + index
        raise IndexError

    def __iter__ (self) :
        return my_range.iterator(self.start, self.stop)

    def __len__ (self) :
        return self.stop - self.start

def test1 () :
    x = my_range(2, 2)
    assert len(x) == 0
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert     hasattr(x, "__getitem__")
    p = iter(x)
    assert     hasattr(p, "__next__")
    assert     hasattr(p, "__iter__")
    assert not hasattr(p, "__getitem__")
    assert iter(p) is p
    assert not list(x)
    y = my_range(2, 2)
    assert x is not y
    assert x ==     y

def test2 () :
    x = my_range(2, 3)
    assert len(x)  == 1
    assert list(x) == [2]
    assert list(x) == [2]

def test3 () :
    x = my_range(2, 4)
    assert len(x)  == 2
    assert list(x) == [2, 3]
    assert list(x) == [2, 3]

def test4 () :
    x = my_range(2, 5)
    assert len(x)  == 3
    assert list(x) == [2, 3, 4]
    assert list(x) == [2, 3, 4]

def test5 () :
    x = my_range(2, 5)
    assert x[0]  == 2
    assert x[1]  == 3
    assert x[2]  == 4
    assert x[-1] == 4
    assert x[-2] == 3
    assert x[-3] == 2
    try :
        assert x[3] == 0
        assert False
    except IndexError :
        pass
    try :
        assert x[-4] == 1
        assert False
    except IndexError :
        pass

def main () :
    print("Range1.py")
    test1()
    test2()
    test3()
    test4()
    test5()
    print("Done.")

if __name__ == "__main__" : #pragma: no cover
    main()
