#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------------
# UnitTests3.py
# -------------

# https://docs.python.org/3/library/unittest.html
# https://docs.python.org/3/library/unittest.html#assert-methods

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test1 (self) :
        self.assertEqual(cycle_length( 1), 1)

    def test2 (self) :
        self.assertEqual(cycle_length( 5), 6)

    def test3 (self) :
        self.assertEqual(cycle_length(10), 7)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
# -v: verbose
% ./UnitTests3.py -v
test1 (__main__.MyUnitTests.test1) ... ok
test2 (__main__.MyUnitTests.test2) ... ok
test3 (__main__.MyUnitTests.test3) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK



# -m: module
# -v: verbose
% python3 -m unittest UnitTests3 -v
test1 (UnitTests3.MyUnitTests.test1) ... ok
test2 (UnitTests3.MyUnitTests.test2) ... ok
test3 (UnitTests3.MyUnitTests.test3) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK



# -m: module
# -v: verbose
% python3 -m unittest UnitTests3.MyUnitTests -v
test1 (UnitTests3.MyUnitTests.test1) ... ok
test2 (UnitTests3.MyUnitTests.test2) ... ok
test3 (UnitTests3.MyUnitTests.test3) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK



# -m: module
# -v: verbose
% python3 -m unittest UnitTests3.MyUnitTests.test2 -v
test2 (UnitTests3.MyUnitTests.test2) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
"""
