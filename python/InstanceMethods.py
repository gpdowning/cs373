#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# ------------------
# InstanceMethods.py
# ------------------

class A :
    cv = 0

    def __init__ (self) :
        # cv    += 1 # UnboundLocalError: cannot access local variable 'cv' where it is not associated with a value
        A.cv    += 1
        self.cv += 0 # misleading

        # A.iv  = 1  # AttributeError: type object 'A' has no attribute 'iv'. Did you mean: 'cv'?
        self.iv = 1

    def im (self) :
        A.cv    += 1
        self.iv += 1

        # sm()         # NameError: name 'sm' is not defined
        A.sm()
        self.sm()      # misleading

        # cm()         # NameError: name 'cm' is not defined
        A.cm()
        self.cm()      # misleading

    @classmethod
    def cm (cls) :
        pass

    @staticmethod
    def sm () :
        pass

def test1 () :
    # assert cv   == 0 # NameError: name 'cv' is not defined
    assert A.cv == 0

    x = A()
    assert A.cv == 1
    assert x.cv == 1 # misleading

def test2 () :
    # assert iv   == 0 # NameError: name 'iv' is not defined
    # assert A.iv == 1 # AttributeError: type object 'A' has no attribute 'iv'

    x = A()
    assert A.cv == 2
    assert x.iv == 1

def test3 () :
    # im()           # NameError: name 'im' is not defined
    # A.im()         # TypeError: im() missing 1 required positional argument: 'self'

    x = A()
    assert A.cv == 3
    assert x.iv == 1

    x.im()
    assert A.cv == 4
    assert x.iv == 2

    A.im(x)          # methods are really just functions
    assert A.cv == 5
    assert x.iv == 3

def test4 () :
    m = map(A.im, [A(), A(), A()])
    assert m

def main () :
    print("InstanceMethods.py")
    for n in range(4) :
        eval("test" + str(n + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
