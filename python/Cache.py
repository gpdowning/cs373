#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement
# pylint: disable = too-few-public-methods

# --------
# Cache.py
# --------

def cache (f) :
    d = {}
    def g (n) :
        if n not in d :
            d[n] = f(n)
        return d[n]
    return g

class cache_class :
    def __init__ (self, f) :
        self.d = {}
        self.f = f

    def __call__ (self, n) :
        if n not in self.d :
            self.d[n] = self.f(n)
        return self.d[n]

@cache
def fibonacci (n) :
    if n < 2 :
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)

def test1 () :
    assert fibonacci(0) == 0

def test2 () :
    assert fibonacci(1) == 1

def test3 () :
    assert fibonacci(2) == 1

def test4 () :
    assert fibonacci(3) == 2

def test5 () :
    assert fibonacci(4) == 3

def test6 () :
    assert fibonacci(5) == 5

def main () :
    print("Cache.py")
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    """
    n = int(input())
    eval("test" + str(n) + "()")
    print("Done.")
    """

if __name__ == "__main__" : #pragma: no cover
    main()
