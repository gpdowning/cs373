#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ----------
# Reduce2.py
# ----------

# https://docs.python.org/3/library/functools.html

import operator

"""
v <bf> a[0] <bf> a[1] <bf> a[2] <bf> ... <bf> a[n-1]
or
a[0] <bf> a[1] <bf> a[2] <bf> ... <bf> a[n-1]
"""

def my_reduce (bf, xs, v = None) :
    if not xs and v is None :
        raise TypeError
    if v is None :
        v, *xs = xs
    for x in xs :
        v = bf(v, x)
    return v

def test1 () :
    # 0 + 2 + 3 + 4
    # add(add(add(0, 2), 3), 4)
    assert my_reduce(operator.add, (2, 3, 4),          0)  ==  9

def test2 () :
    # 1 * 2 * 3 * 4
    # mul(mul(mul(1, 2), 3), 4)
    assert my_reduce(operator.mul, {2, 3, 4},          1)  == 24

def test3 () :
    # 0 - 2 - 3 - 4
    # sub(sub(sub(0, 2), 3), 4)
    assert my_reduce(operator.sub, [2, 3, 4],          0)  == -9

def test4 () :
    # [] <bf> 2 <bf> 3 <bf> 4
    assert my_reduce(lambda v, x : [*v, x], [2, 3, 4], []) == [2, 3, 4]

def test5 () :
    # 2 + 3 + 4
    # add(add(2, 3), 4)
    assert my_reduce(operator.add, (2, 3, 4),           )  ==  9

def test6 () :
    # 2 * 3 * 4
    # mul(mul(2, 3), 4)
    assert my_reduce(operator.mul, {2, 3, 4},           )  == 24

def test7 () :
    # 2 - 3 - 4
    # sub(sub(2, 3), 4)
    assert my_reduce(operator.sub, [2, 3, 4],           )  == -5

def test8 () :
    try :
        my_reduce(None, [])
        assert False
    except TypeError :
        pass

def main () :
    print("Reduce.py")
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    test7()
    test8()
    """
    n = int(input())
    eval("test" + str(n) + "()")
    """
    print("Done.")

if __name__ == "__main__" : #pragma: no cover
    main()
