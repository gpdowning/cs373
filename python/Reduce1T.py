#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -----------
# Reduce1T.py
# -----------

# https://docs.python.org/3.6/library/functools.html

import functools
import operator
import typing
import unittest

"""
v <bf> a[0] <bf> a[1] <bf> a[2] <bf> ... <bf> a[n-1]
"""

T1 = typing.TypeVar("T1")
T2 = typing.TypeVar("T2")

def reduce_for (
        bf: typing.Callable[[T1, T2], T1],
        xs: typing.Iterable[T2],
        v:  T1) \
        -> T1 :
    x: T2
    for x in xs :
        v = bf(v, x)
    return v

def reduce_recursive (
        bf: typing.Callable[[T1, T2], T1],
        xs: typing.Iterable[T2],
        v:  T1) \
        -> T1 :
    if not xs :
        return v
    y: T2
    *xs, y = xs
    return bf(reduce_recursive(bf, xs, v), y)

class MyUnitTests (unittest.TestCase) :
    def setUp (self) -> None :
        self.a: list = [
            reduce_for,
            reduce_recursive,
            functools.reduce]

    def test1 (self) :
        # 0 + 2 + 3 + 4
        # add(add(add(0, 2), 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.add, (2, 3, 4), 0), 9)

    def test2 (self) :
        # 1 * 2 * 3 * 4
        # mul(mul(mul(1, 2), 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.mul, {2, 3, 4}, 1), 24)

    def test3 (self) :
        # 0 - 2 - 3 - 4
        # sub(sub(sub(0, 2), 3), 4)
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(operator.sub, {2, 3, 4}, 0), -9)

    def test4 (self) :
        # [] <bf> 2 <bf> 3 <bf> 4
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(lambda v, x : [*v, x], [2, 3, 4], []), [2, 3, 4])

    def test5 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(None, [], 3),  3)

if __name__ == "__main__" : #pragma: no cover
    unittest.main()
