#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ------------
# Coverage1.py
# ------------

# https://coverage.readthedocs.org

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test (self) :
        self.assertEqual(cycle_length(1), 1)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
% which coverage
/usr/local/bin/coverage



%
Coverage.py, version 7.6.10 with C extension
Full documentation is at https://coverage.readthedocs.io/en/7.6.10



% coverage help
Coverage.py, version 7.6.10 with C extension
Measure, collect, and report on code coverage in Python programs.

usage: coverage <command> [options] [args]

Commands:
    annotate    Annotate source files with execution information.
    combine     Combine a number of data files.
    debug       Display information about the internals of coverage.py
    erase       Erase previously collected coverage data.
    help        Get help on using coverage.py.
    html        Create an HTML report.
    json        Create a JSON report of coverage results.
    lcov        Create an LCOV report of coverage results.
    report      Report coverage stats on modules.
    run         Run a Python program and measure code execution.
    xml         Create an XML report of coverage results.

Use "coverage help <command>" for detailed help on any command.
Full documentation is at https://coverage.readthedocs.io/en/7.6.10



% python3 -m coverage run Coverage1.py
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK



% ls -al .coverage
-rw-r--r--@ 1 downing  staff  53248 Jan 22 19:18 .coverage



% python3 -m coverage report
Name           Stmts   Miss  Cover
----------------------------------
Coverage1.py      17      5    71%
----------------------------------
TOTAL             17      5    71%
"""
