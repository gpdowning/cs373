#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------
# imports
# -------

import typing

# -------------
# FactorialT.py
# -------------

# https://docs.python.org/3.6/library/math.html

import functools
import math
import operator
import timeit
import unittest

# recursive procedure
# recursive process
def factorial_recursive (n: int) -> int :
    assert n >= 0
    if n < 2 :
        return 1
    return n * factorial_recursive(n - 1)

# recursive procedure
# iterative process
def factorial_tail_recursive (n: int, v: int = 1) -> int :
    assert n >= 0
    assert v >= 1
    if n < 2 :
        return v
    return factorial_tail_recursive(n - 1 , n * v)

# iterative procedure
# iterative process
def factorial_while (n: int) -> int :
    assert n >= 0
    v: int = 1
    while n > 1 :
        v *= n
        n -= 1
    return v

# iterative procedure
# iterative process
def factorial_for_range (n: int) -> int :
    assert n >= 0
    v: int = 1
    i: int
    for i in range(1, n + 1) :
        v *= i
    return v

# iterative procedure
# iterative process
def factorial_range_iterator (n: int) -> int :
    assert n >= 0
    v: int                  = 1
    p: typing.Iterator[int] = iter(range(1, n + 1))
    try :
        while True :
            i: int  = next(p)
            v      *= i
    except StopIteration :
        pass
    return v

# iterative procedure
# iterative process
def factorial_range_reduce_1 (n: int) -> int :
    assert n >= 0
    return functools.reduce(lambda x, y : x * y, range(1, n + 1), 1)

# iterative procedure
# iterative process
def factorial_range_reduce_2 (n: int) -> int :
    assert n >= 0
    return functools.reduce(operator.mul, range(1, n + 1), 1)

class MyUnitTests (unittest.TestCase) :
    def setUp (self) -> None :
        self.a: list = [
            factorial_recursive,
            factorial_tail_recursive,
            factorial_while,
            factorial_for_range,
            factorial_range_iterator,
            factorial_range_reduce_1,
            factorial_range_reduce_2,
            math.factorial]

    def test1 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(0), 1)

    def test2 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(1), 1)

    def test3 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(2), 2)

    def test4 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(3), 6)

    def test5 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(4), 24)

    def test6 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(5), 120)

    def test7 (self) -> None :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                print()
                print(f.__name__)
                t: float
                if f.__name__ == "factorial" :
                    t = timeit.timeit(
                        "math.factorial(100)",
                        "import math",
                        number = 1000)
                else :
                    t = timeit.timeit(
                        "__main__." + f.__name__ + "(100)",
                        "import __main__",
                        number = 1000)
                print(f"{t*1000:.2f} milliseconds")

if __name__ == "__main__" : #pragma: no cover
    unittest.main()

""" #pragma: no cover
% FactorialT.py
......
factorial_recursive
61.83 milliseconds

factorial_tail_recursive
72.54 milliseconds

factorial_while
36.77 milliseconds

factorial_for_range
25.77 milliseconds

factorial_range_iterator
38.35 milliseconds

factorial_range_reduce_1
36.70 milliseconds

factorial_range_reduce_2
6.10 milliseconds

factorial
0.92 milliseconds
.
----------------------------------------------------------------------
Ran 7 tests in 0.244s

OK
"""
