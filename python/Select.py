#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ---------
# Select.py
# ---------

# http://en.wikipedia.org/wiki/Selection_(relational_algebra)

def select (r, f) :
    for d in r :
        if f(d) :
            yield d

def select2 (r, f) :
    return (d for d in r if f(d))

def select3 (r, f) :
    return filter(f, r)

class select4 :
    def __init__ (self, r, f) :
        self.p = iter(r)
        self.f = f

    def __iter__ (self) :
        return self

    def __next__ (self) :
        while True :
            d = next(self.p)
            if self.f(d) :
                return d

class select5 :
    def __init__ (self, r, f) :
        self.p = iter(r)
        self.f = f

    def __iter__ (self) :
        return self

    def __next__ (self) :
        d = next(self.p)
        if self.f(d) :
            return d
        return next(self)

def test1 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : False)
    assert not list(x)

def test2 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : True)
    assert                            \
        list(x)                       \
        ==                            \
        [{"A" : 1, "B" : 4, "C" : 3},
         {"A" : 2, "B" : 5, "C" : 2},
         {"A" : 3, "B" : 6, "C" : 1}]
    assert not list(x)

def test3 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : d["B"] > 4)
    assert                               \
        list(x)                          \
        ==                               \
        [{'A': 2, 'B': 5, 'C': 2},
         {'A': 3, 'B': 6, 'C': 1}]
    assert not list(x)

def test4 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : d["A"] > d["C"])
    assert                                    \
        list(x)                               \
        ==                                    \
        [{'A': 3, 'B': 6, 'C': 1}]
    assert not list(x)

def main () :
    print("Select.py")
    test1()
    test2()
    test3()
    test4()
    """
    n = int(input())
    eval("test" + str(n) + "()")
    """
    print("Done.")

if __name__ == "__main__" : #pragma: no cover
    main()
