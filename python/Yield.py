#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = no-member

# --------
# Yield.py
# --------

# https://docs.python.org/3.9/reference/simple_stmts.html?highlight=yield#grammar-token-yield-stmt

import types

def f () :
    yield 2
    yield 3
    yield 4
    return "Nothing to be done."

def test1 () :
    p = f()
    assert isinstance(p, types.GeneratorType)
    assert     hasattr(p, "__next__")
    assert     hasattr(p, "__iter__")
    assert not hasattr(p, "__getitem__")
    assert not hasattr(p, "__len__")
    assert iter(p) is p
    n = next(p)
    assert n == 2
    n = next(p)
    assert n == 3
    n = next(p)
    assert n == 4
    try :
        n = next(p)
    except StopIteration as e :
        assert e.value == "Nothing to be done."

def test2 () :
    p = f()
    assert list(p) == [2, 3, 4]
    assert not list(p)

def g () :
    for v in [2, 3, 4] :
        yield v
    return "Nothing to be done."

def test3 () :
    p = g()
    assert iter(p) is p
    n = next(p)
    assert n == 2
    n = next(p)
    assert n == 3
    n = next(p)
    assert n == 4
    try :
        n = next(p)
    except StopIteration as e :
        assert e.value == "Nothing to be done."

def test4 () :
    p = g()
    assert list(p) == [2, 3, 4]
    assert not list(p)

def main () :
    print("Yield.py")
    for i in range(4) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
