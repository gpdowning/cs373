#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ------------
# Factorial.py
# ------------

# https://docs.python.org/3.6/library/math.html

def my_factorial (n) :
    assert n >= 0
    if n == 0 :
        return 1
    return n * my_factorial(n - 1)

def my_factorial_while (n) :
    assert n >= 0
    v = 1
    while n > 1 :
        v *= n
        n -= 1
    return v

def my_factorial_for_range (n) :
    assert n >= 0
    v = 1
    for i in range(1, n + 1) :
        v *= i
    return v

def test1 () :
    assert my_factorial(0) == 1

def test2 () :
    assert my_factorial(1) == 1

def test3 () :
    assert my_factorial(2) == 2

def test4 () :
    assert my_factorial(3) == 6

def test5 () :
    assert my_factorial(4) == 24

def test6 () :
    assert my_factorial(5) == 120

def main () :
    print("Factorial.py")
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    """
    n = int(input())
    eval("test" + str(n) + "()")
    """
    print("Done.")

if __name__ == "__main__" : #pragma: no cover
    main()
