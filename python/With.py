#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = consider-using-with
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement
# pylint: disable = unspecified-encoding
# pylint: disable = unnecessary-dunder-call

# -------
# With.py
# -------

# https://docs.python.org/3/reference/compound_stmts.html#the-with-statement

def test1 () :
    print("test1:")
    print("opening file")
    f = open("With.py")
    try :
        assert f.readline() == "#!/usr/bin/env python3\n"
    finally :
        print("closing file")
        f.close()
    print()

def test2 () :
    print("test2:")
    with open("With.py") as f :
        assert f.readline() == "#!/usr/bin/env python3\n"
    print()

class my_open :
    def __init__ (self, s) :
        self.s = s
        self.f = None

    def __enter__ (self) :
        print("opening file")
        self.f = open(self.s)
        return self.f

    def __exit__ (self, *t) :
        print("closing file")
        self.f.close()

def test3 () :
    print("test3:")
    x = my_open("With.py")
    f = x.__enter__()
    try :
        assert f.readline() == "#!/usr/bin/env python3\n"
    finally :
        x.__exit__()
    print()

def test4 () :
    print("test4:")
    with my_open("With.py") as f :
        assert f.readline() == "#!/usr/bin/env python3\n"

def main () :
    for i in range(4) :
        eval("test" + str(i + 1) + "()")

if __name__ == "__main__" :
    main()

"""
% With.py
With.py
test1:
opening file
closing file

test2:

test3:
opening file
closing file

test4:
opening file
closing file
"""
