#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ----------
# IsPrime.py
# ----------

import math

def is_prime (n: int) -> bool :
    assert n > 0
    if (n == 1) or ((n % 2) == 0) :
        return False
    for i in range(3, int(math.sqrt(n))) :
        if (n % i) == 0 :
            return False
    return True

def test1 () :
    assert not is_prime(1)

def test2 () :
    assert not is_prime(2)

def test3 () :
    assert is_prime(3)

def test4 () :
    assert not is_prime(4)

def test5 () :
    assert is_prime(5)

def test6 () :
    assert is_prime(7)

def test7 () :
    assert is_prime(9)

def test8 () :
    assert not is_prime(27)

def test9 () :
    assert is_prime(29)

def main () :
    print("IsPrime.py")
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
    test7()
    test8()
    test9()
    """
    n = int(input())
    eval("test" + str(n) + "()")
    """
    print("Done.")

if __name__ == "__main__" : #pragma: no cover
    main()
