#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# ----------------
# StaticMethods.py
# ----------------

class A :
    cv = 0

    def __init__ (self) :
        A.cv    += 1
        self.iv  = 1

    def im (self) :
        pass

    @classmethod
    def cm (cls) :
        pass

    @staticmethod
    def sm () :
        # cv += 1 # UnboundLocalError: cannot access local variable 'cv' where it is not associated with a value
        A.cv += 1

        # im()    # NameError: name 'im' is not defined
        # A.im()  # TypeError: im() missing 1 required positional argument: 'self'

        # cm()    # NameError: name 'cm' is not defined
        A.cm()

def test1 () :
    assert A.cv == 0

    A.sm()
    assert A.cv == 1

def test2 () :
    x = A()
    assert A.cv == 2
    assert x.iv == 1

    A.sm()
    x.sm()           # misleading
    assert A.cv == 4
    assert x.iv == 1

class MyList :
    def __init__ (self, a) :
        self.a = a

    @staticmethod
    def from_size (s) :
        return MyList(s * [0])

    @staticmethod
    def from_size_value (s, v) :
        return MyList(s * [v])

def test3 () :
    x = MyList.from_size(2)
    assert isinstance(x, MyList)
    assert x.a == [0, 0]

def test4 () :
    x = MyList.from_size_value(3, 4)
    assert isinstance(x, MyList)
    assert x.a == [4, 4, 4]

def main () :
    print("StaticMethods.py")
    for n in range(4) :
        eval("test" + str(n + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
