#!/usr/bin/env python3

# pylint: disable = cell-var-from-loop
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = unnecessary-lambda-assignment
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# ------------
# Functions.py
# ------------

def square_def (v) :
    return v ** 2

def test1 () :
    a = [2, 3, 4]
    assert hasattr(square_def, "__call__")
    m = map(square_def, a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





square_lambda = lambda v : v ** 2

def test2 () :
    a = [2, 3, 4]
    assert hasattr(square_lambda, "__call__")
    m = map(square_lambda, a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





class square_class :
    def __call__ (self, v) :
        return v ** 2

def test3 () :
    a = [2, 3, 4]
    assert hasattr(square_class,   "__call__")
    assert hasattr(square_class(), "__call__")
    m = map(square_class(), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def pow_def (p) :
    def f (v) :
        return v ** p
    return f

def test4 () :
    a = [2, 3, 4]
    assert pow_def(2)(2) == 4
    m = map(pow_def(2), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def pow_lambda (p) :
    return lambda v : v ** p

def test5 () :
    a = [2, 3, 4]
    assert pow_lambda(2)(2) == 4
    m = map(pow_lambda(2), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





class pow_class :
    def __init__ (self, p) :
        self.p = p

    def __call__ (self, v) :
        return v ** self.p

def test6 () :
    a = [2, 3, 4]
    assert pow_class(2)(2) == 4
    m = map(pow_class(2), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def test7 () :
    a     = [2, 3, 4]
    n     = [1]
    m     = map(lambda v : v ** n[0], a)
    a    += [5]
    n[0]  = 2
    assert list(m) == [4, 9, 16, 25]
    assert not list(m)

def test8 () :
    a = [2, 3, 4]
    n = [1]
    m = map(lambda v : v ** n[0], a)
    a = [2, 3, 4, 5]
    n = [2]
    assert list(m) == [4, 9, 16]
    assert not list(m)

def test9 () :
    a = [2, 3, 4]
    f = [lambda v : v ** n for n in range(3)]
    p = iter(f)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)

def test10 () :
    a = [2, 3, 4]
    f = (lambda v : v ** n for n in range(3))
    p = iter(f)
    m = map(next(p), a)
    assert list(m) == [1, 1, 1]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [2, 3, 4]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def main () :
    print("Functions.py")
    for i in range(10) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
