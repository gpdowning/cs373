#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ---------
# Reduce.py
# ---------

# https://docs.python.org/3.6/library/functools.html

import functools
import operator

def my_sum (a) :
    return functools.reduce(operator.add, a, 0)

def test1 () -> None :
    assert my_sum([2, 3, 4]) == 9



def my_product (a) :
    return functools.reduce(operator.mul, a, 1)

def test2 () -> None :
    assert my_product([2, 3, 4]) == 24



def my_factorial (n) :
    return functools.reduce(operator.mul, range(1, n + 1), 1)

def test3 () -> None :
    assert my_factorial(5) == 120



def my_map (uf, a) :
    return functools.reduce(lambda v, w : [*v, uf(w)], a, [])

def test4 () -> None :
    assert my_map(lambda v : v * v, [2, 3, 4]) == [4, 9, 16]



def my_in (u, a) :
    return functools.reduce(lambda v, w : True if w == u else v, a, False)

def test5 () -> None :
    assert     my_in(3, [2, 3, 4])
    assert not my_in(5, [2, 3, 4])



def my_reverse (a) :
    return functools.reduce(lambda v, w : [w, *v], a, [])

def test6 () -> None :
    assert my_reverse([2, 3, 4]) == [4, 3, 2]



def my_filter (up, a) :
    return functools.reduce(lambda v, w : [*v, w] if up(w) else v, a, [])

def test7 () -> None :
    assert my_filter(lambda v : v % 2, [2, 3, 4]) == [3]



def main () :
    print("Reduce.py")
    for i in range(7) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()
