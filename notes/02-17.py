# -----------
# Mon, 17 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 16 Feb: Blog  #5
    Sun, 16 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 17 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Fri, 21 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    iteration
    types
    yield

this time
    comprehensions
"""





"""
. A public GitLab repo with an issue tracker with at least 30 resolved issues with issue labels.

. Use of Bootstrap.

. Hosting on AWS.

. A URL with https.

. A splash page

. An about page with info about each member, including GitLab stats and an even number of commits and issues.

. Three model pages, each with a grid of three cards. Each card has an image and five filterable/sortable attributes.

. Nine instance pages with lots of multimedia & text and connections to the instances of the other models. The connections with cards are identical to those on the model pages.

. Postman documentation.

. A technical report.

. Five user stories to their developer team.
"""





# -----------------
# Comprehensions.py
# -----------------

def test1 () :
    a = [2, 3, 4]
    b = []
    for v in a :
        b += [v * v]
    assert isinstance(b, list)
    assert a       == [2, 3,  4]
    assert b       == [4, 9, 16]

def test2 () :
    a = [2, 3, 4]
    b = [v * v for v in a]       # list comprehension, O(n), eager
    assert isinstance(b, list)
    assert a       == [2, 3,  4]
    assert b       == [4, 9, 16]

def test3 () :
    a = [(2, 3), [4, 5], (6, 7)]
    b = [u + v for u, v in a]
    assert b == [5, 9, 13]

def test4 () :
    a = [2, 3, 4]
    g = (v * v for v in a)                    # generator, O(1), lazy
    assert isinstance(g, types.GeneratorType)
    assert     hasattr(g, "__next__")
    assert     hasattr(g, "__iter__")
    assert not hasattr(g, "__getitem__")
    assert not hasattr(g, "__len__")
    assert iter(g) is g
    assert a       == [2, 3,  4]
    assert list(g) == [4, 9, 16]
    assert not list(g)

def test5 () :
    a  = [2, 3, 4]
    g  = (v * v for v in a)          # capture the list, NOT a
    a += [5]                         # change  the list, NOT a
    assert a       == [2, 3,  4,  5]
    assert list(g) == [4, 9, 16, 25]
    assert not list(g)
    a += [5]
    assert not list(g)

def test6 () :
    a = [2, 3, 4]
    g = (v * v for v in a)           # capture the list, NOT a
    a = [2, 3, 4, 5]                 # change a,         NOT the list
    assert a       == [2, 3,  4,  5]
    assert list(g) == [4, 9, 16]
    assert not list(g)

def test7 () :
    a = [2, 3, 4]
    m = map(lambda v : v * v, a)
    assert isinstance(m, map)
    assert     hasattr(m, "__next__")
    assert     hasattr(m, "__iter__")
    assert not hasattr(m, "__getitem__")
    assert not hasattr(m, "__len__")
    assert iter(m) is m
    assert a       == [2, 3,  4]
    assert list(m) == [4, 9, 16]
    assert not list(m)

def test8 () :
    a = [2, 3, 4]
    m = map(lambda v : v * v, a)     # capture the list, NOT a
    a += [5]                         # change  the list, NOT a
    assert a       == [2, 3,  4,  5]
    assert list(m) == [4, 9, 16, 25]
    assert not list(m)
    a += [5]
    assert not list(m)

def test9 () :
    a = [2, 3, 4]
    b = [5, 6, 7]
    m = map(lambda x, y : x + y, a, b)
    assert list(m) == [7, 9, 11]
    assert not list(m)

def test10 () :
    a = [2, 3, 4]
    b = [5, 6, 7]
    z = zip(a, b)
    m = (x + y for x, y in z)
    assert list(m) == [7, 9, 11]
    assert not list(z)
    assert not list(m)

def test11 () :
    a = [2, 3, 4, 5, 6]
    b = []
    for v in a :
        if v % 2 :
            b += [v * v]
    assert a == [2, 3, 4,  5,  6]
    assert b == [   9,    25]

def test12 () :
    a = [2, 3, 4, 5, 6]
    b = [v * v for v in a if v % 2]
    assert a == [2, 3, 4,  5,  6]
    assert b == [   9,    25]

def test13 () :
    a = [2, 3, 4, 5, 6]
    g = (v * v for v in a if v % 2)
    assert a       == [2,  3,  4,  5,  6]
    assert list(g) == [    9,     25]
    assert not list(g)

def test14 () :
    a = [2, 3, 4, 5, 6]
    f = filter(lambda v : v % 2, a)
    assert isinstance(f, filter)
    assert     hasattr(f, "__next__")
    assert     hasattr(f, "__iter__")
    assert not hasattr(f, "__getitem__")
    assert not hasattr(f, "__len__")
    assert iter(f) is f
    m = map(lambda v : v * v, f)
    assert a       == [2,  3,  4,  5,  6]
    assert list(m) == [    9,     25]
    assert not list(f)
    assert not list(m)

def test15 () :
    a = [2, 3, 4, 5, 6]
    f = (v for v in a if v % 2)
    m = (v * v for v in f)
    assert a       == [2,  3,  4,  5,  6]
    assert list(m) == [    9,     25]
    assert not list(f)
    assert not list(m)

def test16 () :
    a = [2, 3, 4]
    b = [4, 5]
    c = []
    for v in a :
        for w in b :
            c += [v + w]
    assert a == [2, 3, 4]
    assert b == [4, 5]
    assert c == [2+4, 2+5, 3+4, 3+5, 4+4, 4+5]
    assert c == [  6,   7,   7,   8,   8,   9]

def test17 () :
    a = [2, 3, 4]
    b = [4, 5]
    c = [v + w for v in a for w in b]
    assert a == [2, 3, 4]
    assert b == [4, 5]
    assert c == [2+4, 2+5, 3+4, 3+5, 4+4, 4+5]
    assert c == [  6,   7,   7,   8,   8,   9]

def test18 () :
    a = [2, 3, 4]
    b = [4, 5]
    g = (v + w for v in a for w in b)
    assert a       == [2, 3, 4]
    assert b       == [4, 5]
    assert list(g) == [2+4, 2+5, 3+4, 3+5, 4+4, 4+5]
    assert not list(g)
