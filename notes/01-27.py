# -----------
# Mon, 27 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Feb: Blog  #3
    Sun, 2 Feb: Paper #3

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/

    Fri, 31 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/
"""

"""
last time
    handling errors without exceptions
    use the return
    use a global
    use an argument (be careful)

this time
    exceptions
"""





def f (...) :
    ...
    if (<something wrong>)
        raise E(...)       # create an instance of type E and raise it
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except E as e :
        <something wrong>
    ...

"""
f does NOT raise E
rest of f
rest of try
NO except
rest of g
"""

"""
f does raise E
NO rest of f
NO rest of try
except E
rest of g
"""




def f (...) :
    ...
    if (<something wrong>)
        raise E2
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except E as e :
        <something wrong>
    ...

"""
f does raise E2
NO rest of f
NO rest of try
NO except E
NO rest of g
"""





def f (...) :
    ...
    if (<something wrong>)
        raise tiger(...)

    if (<something wrong>)
        raise bear(...)
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except tiger as e :    # put children first
        <something wrong>
    except mammal as e :
        <something wrong>
    else :                 # only if NO raise
        ...
    finally :              # ALWAYS
        ...
    ...





#!/usr/bin/env python3

# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = literal-comparison
# pylint: disable = missing-docstring

# -------------
# Exceptions.py
# -------------

# https://docs.python.org/3/library/exceptions.html

def f (b: bool) -> int :
    if b :
        raise NameError("abc")
    return 0

def test1 () -> None :
    try :
        assert f(False) == 0
    except NameError :       # pragma: no cover
        assert False
    else :                   # no exception vs. raised and handled, but not with break, continue, return
        pass
    finally :                # always, even with break, continue, return
        pass

def test2 () -> None :
    try :
        assert f(True) == 1
        assert False                         # pragma: no cover
    except NameError as e :
        assert isinstance(e,      NameError)
        assert isinstance(e.args, tuple)
        assert len(e.args)  ==     1
        assert e.args       is not ("abc",)
        assert e.args       ==     ("abc",)
    else :                                   # no exception vs. raised and handled, but not with break, continue, return
        assert False                         # pragma: no cover
    finally :                                # always, even with break, continue, return
        pass

def test3 () -> None :
    assert issubclass(NameError,     Exception)
    assert issubclass(Exception,     BaseException)
    assert issubclass(BaseException, object)

def main () -> None :
    print("Exceptions.py")
    for i in range(3) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" :
    main()





a = [2, 3, 4]
print(type(a)) # list, mutable in size and content

a = [2]
print(type(a)) # list





a = (2, 3, 4)
print(type(a)) # tuple, immutable in size and content

a = (2)
print(type(a)) # int

a = (2,)
print(type(a)) # tuple




"""
Java

==      compares identity
.equals compares content, if you don't have one, it will compare identity
"""

a = [2, 3, 4]
b = [2, 3, 4]
print(a == b) # compares content,  true, if you don't have one, it will compare identity
print(a is b) # compares identity, false
