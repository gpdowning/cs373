# -----------
# Fri, 31 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Feb: Blog  #3
    Sun, 2 Feb: Paper #3

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 31 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/
"""

"""
last time
    recursion
    iteration

this time
    factorial()
    reduce()
"""





class A :
    pass

x = A()
y = A()
print(x is y) # false
print(x == y) # false # x.__eq__(y)





class A :
    def __eq__ (self, rhs) :
        return True

x = A()
y = A()
print(x is y) # false
print(x == y) # true # x.__eq__(y)




# -------------
# FactorialT.py
# -------------

# iterative procedure
# iterative process
def factorial_while (n: int) -> int :
    assert n >= 0
    v: int = 1
    while n > 1 :
        v *= n
        n -= 1
    return v

# iterative procedure
# iterative process
def factorial_range_for (n: int) -> int :
    assert n >= 0
    v: int = 1
    i: int
    for i in range(1, n + 1) :
        v *= i
    return v

# iterative procedure
# iterative process
def factorial_range_iterator (n: int) -> int :
    assert n >= 0
    v: int                  = 1
    p: typing.Iterator[int] = iter(range(1, n + 1))
    try :
        while True :
            i: int  = next(p)
            v      *= i
    except StopIteration :
        pass
    return v





"""
reduce (bf, a, v)

bf: binary function
a:  an iterable
v:  a seed

v <bf> a0 <bf> a1 <bf> ... <bf> an-1
"""





"""
bf: +
a:  a seq of integers
v:  0
returns the sum
"""





"""
bf: *
a:  a seq of integers
v:  1
returns the product
"""





"""
bf: *
a:  range(1, n+1)
v:  1
returns the factorial
"""





# iterative procedure
# iterative process
def factorial_range_reduce_1 (n: int) -> int :
    assert n >= 0
    return functools.reduce(lambda x, y : x * y, range(1, n + 1), 1)

# iterative procedure
# iterative process
def factorial_range_reduce_2 (n: int) -> int :
    assert n >= 0
    return functools.reduce(operator.mul, range(1, n + 1), 1)

class MyUnitTests (unittest.TestCase) :
    def setUp (self) -> None :
        self.a: list = [
            factorial_recursion,
            factorial_tail_recursion,
            factorial_while,
            factorial_range_for,
            factorial_range_iterator,
            factorial_range_reduce_1,
            factorial_range_reduce_2,
            math.factorial]

    def test0 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(0), 1)

    def test1 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(1), 1)

    def test2 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(2), 2)

    def test3 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(3), 6)

    def test4 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(4), 24)

    def test5 (self) :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                self.assertEqual(f(5), 120)

    def test6 (self) -> None :
        for f in self.a :
            with self.subTest(msg=f.__name__) :
                print()
                print(f.__name__)
                t: float
                if f.__name__ == "factorial" :
                    t = timeit.timeit(
                        "math.factorial(100)",
                        "import math",
                        number = 1000)
                else :
                    t = timeit.timeit(
                        "__main__." + f.__name__ + "(100)",
                        "import __main__",
                        number = 1000)
                print(f"{t*1000:.2f} milliseconds")

if __name__ == "__main__" : #pragma: no cover
    unittest.main()

""" #pragma: no cover
% FactorialT.py
......
factorial_recursion
61.83 milliseconds

factorial_tail_recursion
72.54 milliseconds

factorial_while
36.77 milliseconds

factorial_range_for
25.77 milliseconds

factorial_range_iterator
38.35 milliseconds

factorial_range_reduce_1
36.70 milliseconds

factorial_range_reduce_2
6.10 milliseconds

factorial
0.92 milliseconds
.
----------------------------------------------------------------------
Ran 7 tests in 0.244s

OK
"""





# ---------
# Reduce.py
# ---------

def my_sum (a) :
    return functools.reduce(operator.add, a, 0)

# 0 + 2 + 3 + 4

def test1 () -> None :
    assert my_sum([2, 3, 4]) == 9





def my_product (a) :
    return functools.reduce(operator.mul, a, 1)

# 1 * 2 * 3 * 4

def test2 () -> None :
    assert my_product([2, 3, 4]) == 24





def my_factorial (n) :
    return functools.reduce(operator.mul, range(1, n + 1), 1)

# 1 * 1 * 2 * 3 * 4 * 5

def test3 () -> None :
    assert my_factorial(5) == 120





def my_map (uf, a) :
    return functools.reduce(lambda v, w : [*v, uf(w)], a, [])

[]         <bf> 2 <bf> 3 <bf> 4
[4]        <bf> 3 <bf> 4
[4, 9]     <bf> 4
[4, 9, 16]

def test4 () -> None :
    assert my_map(lambda v : v * v, [2, 3, 4]) == [4, 9, 16]





def my_in (u, a) :
    return functools.reduce(lambda v, w : True if w == u else v, a, False)

False <bf> 2 <bf> 3 <bf> 4
False <bf> 3 <bf> 4
True  <bf> 4
True

def test5 () -> None :
    assert     my_in(3, [2, 3, 4])
    assert not my_in(5, [2, 3, 4])





def my_reverse (a) :
    return functools.reduce(lambda v, w : [w, *v], a, [])

[]        <bf> 2 <bf> 3 <bf> 4
[4]       <bf> 3 <bf> 4
[4, 3]    <bf> 4
[4, 3, 2]

def test6 () -> None :
    assert my_reverse([2, 3, 4]) == [4, 3, 2]





def my_filter (up, a) :
    return functools.reduce(lambda v, w : [*v, w] if up(w) else v, a, [])

[]  <bf> 2 <bf> 3 <bf> 4
[]  <bf> 3 <bf> 4
[3] <bf> 4
[3]

def test7 () -> None :
    assert my_filter(lambda v : v % 2, [2, 3, 4]) == [3]
