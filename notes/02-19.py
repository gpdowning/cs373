# -----------
# Wed, 19 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 16 Feb: Blog  #5
    Sun, 16 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 21 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 24 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    comprehensions

this time
    comprehensions
    functions
"""





"""
. A public GitLab repo with an issue tracker with at least 30 resolved issues with issue labels.

. Use of Bootstrap and Tailwind.

. Hosting on AWS.

. A URL with https.

. A splash page

. An about page with info about each member, including GitLab stats and an even number of commits and issues.

. Three model pages, each with a grid of three cards. Each card has an image and five filterable/sortable attributes.

. Nine instance pages with lots of multimedia & text and connections to the instances of the other models. The connections with cards are identical to those on the model pages.

. Postman documentation.

. A technical report.

. Five user stories to their developer team.
"""





# -----------------
# Comprehensions.py
# -----------------

def test19 () :
    s = {2, 3, 4}
    t = set()
    for v in s :
        t |= {v * v}
    assert s == {2, 3,  4}
    assert t == {4, 9, 16}

def test20 () :
    s = {2, 3, 4}
    t = {v * v for v in s} # set comprehension, O(n), eager
    assert s == {2, 3,  4}
    assert t == {4, 9, 16}

def test21 () :
    d = {2: "abc", 3: "def", 4: "ghi"}
    e = {}
    for k in d :
        e[k + 1] = d[k] + "xyz"
    assert d == {2: "abc",    3: "def",    4: "ghi"}
    assert e == {3: "abcxyz", 4: "defxyz", 5: "ghixyz"}

def test22 () :
    d = {2: "abc", 3: "def", 4: "ghi"}
    e = {k + 1: d[k] + "xyz" for k in d}                # dict comprehension, O(n), eager
    assert d == {2: "abc",    3: "def",    4: "ghi"}
    assert e == {3: "abcxyz", 4: "defxyz", 5: "ghixyz"}





# ------------
# Functions.py
# ------------

def square_def (v) :
    return v ** 2

x = 2
y = square_def(x)
print(y)          # 4

def test1 () :
    a = [2, 3, 4]
    assert hasattr(square_def, "__call__")
    m = map(square_def, a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





square_lambda = lambda v : v ** 2

x = 2
y = square_lambda(x)
print(y)             # 4

def test2 () :
    a = [2, 3, 4]
    assert hasattr(square_lambda, "__call__")
    m = map(square_lambda, a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





class square_class :
    def __call__ (self, v) :
        return v ** 2

x = square_class() # __init__
y = x(2)           # x.__call__(2)
print(y)           # 4

def test3 () :
    a = [2, 3, 4]
    assert hasattr(square_class,   "__call__")
    assert hasattr(square_class(), "__call__")
    m = map(square_class,   a)                 # no
    m = map(square_class(), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





class square_class :
    def __init__ (self, x) :
        ...

    def __call__ (self, v) :
        return v ** 2

x = square_class(5) # __init__
y = x(2)            # x.__call__(2)
print(y)            # 4

def test3 () :
    a = [2, 3, 4]
    assert hasattr(square_class,   "__call__")
    assert hasattr(square_class(), "__call__")
    m = map(square_class,   a)                 # generate instances of square_class
    m = map(square_class(), a)                 # generate ints
