# -----------
# Fri,  7 Mar
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Mar: Blog  #7
    Sun, 2 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 7 Mar, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 10 Mar, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Mar: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #4: WiCS
    https://www.cs.utexas.edu/~utpc/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    regular expressions
    with
    instance methods
    static methods
    class methods

this time
    relational algebra
    select
"""





"""
relational algebra
"""

"""
algebra
    elements
    operations
    some are closed on some operations
"""





"""
integer arithmetic
    integers
    +, / (open), *, %, -
"""





"""
relational algebra
    relations, tables
    joins
    select
    project
    everything is closed
"""





# ---------
# Select.py
# ---------

def test1 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : False)
    assert not list(x)

def test2 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : True)
    assert                            \
        list(x)                       \
        ==                            \
        [{"A" : 1, "B" : 4, "C" : 3},
         {"A" : 2, "B" : 5, "C" : 2},
         {"A" : 3, "B" : 6, "C" : 1}]
    assert not list(x)

def test3 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : d["B"] > 4)
    assert                               \
        list(x)                          \
        ==                               \
        [{'A': 2, 'B': 5, 'C': 2},
         {'A': 3, 'B': 6, 'C': 1}]
    assert not list(x)

def test4 () :
    r = [
        {"A" : 1, "B" : 4, "C" : 3},
        {"A" : 2, "B" : 5, "C" : 2},
        {"A" : 3, "B" : 6, "C" : 1}]
    x = select(r, lambda d : d["A"] > d["C"])
    assert                                    \
        list(x)                               \
        ==                                    \
        [{'A': 3, 'B': 6, 'C': 1}]
    assert not list(x)





# ---------
# Select.py
# ---------

def select1 (r, f) :
    for d in r :
        if f(d) :
            yield d

def select2 (r, f) :
    return (d for d in r if f(d))

def select3 (r, f) :
    return filter(f, r)

class select4 :
    def __init__ (self, r, f) :
        self.p = iter(r)
        self.f = f

    def __iter__ (self) :
        return self

    def __next__ (self) :
        while True :
            d = next(self.p)
            if self.f(d) :
                return d

class select5 :
    def __init__ (self, r, f) :
        self.p = iter(r)
        self.f = f

    def __iter__ (self) :
        return self

    def __next__ (self) :
        d = next(self.p)
        if self.f(d) :
            return d
        return next(self)
