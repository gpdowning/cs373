# -----------
# Wed, 12 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 16 Feb: Blog  #5
    Sun, 16 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Fri, 14 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 17 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""




"""
last time
    operators
    iteration
    range

this time
    Exercise #4: range
    types
"""





class my_range () :
    class iterator () :
        def __init__ (self, start, stop) :
            self.start = start
            self.stop  = stop

        def __iter__ (self) :
            return self

        def __next__ (self) :
            if self.start >= self.stop :
                raise StopIteration()
            start       = self.start
            self.start += 1
            return start

    def __init__ (self, start, stop) :
        self.start = start
        self.stop  = stop

    def __eq__ (self, rhs) :
        return (self.start == rhs.start) and (self.stop == rhs.stop)

    def __getitem__ (self, index) :
        if 0 <= index < (self.stop - self.start) :
            return self.start + index
        if 0 > index >= (self.start - self.stop) :
            return self.stop + index
        raise IndexError

    def __iter__ (self) :
        return my_range.iterator(self.start, self.stop)

    def __len__ (self) :
        return self.stop - self.start





# --------
# Types.py
# --------

def test1 () -> None :
    b: bool = bool()              # bools are stored by value
    c: bool = False
    assert b is c
    assert isinstance(b,    bool)
    assert isinstance(bool, type)

def test2 () -> None :
    i: int = int()               # ints are store by value here
    j: int = 0
    assert i is j
    i  = 2
    i += 1
    j  = 3
    assert i is j
    i  = 255
    i +=   1
    j  = 256
    assert i is j
    i  = 256
    i +=   1
    j  = 257
    assert i is not j            # ints are stored by address here
    assert i ==     j            # cache: [-5, 256]
    k: int = 257
    assert j is     k            # literal caching
    i =  -4
    i -=  1
    j =  -5
    assert i is j
    i =  -5
    i -=  1
    j =  -6
    assert i is not j            # ints are stored by address here
    assert i ==     j            # cache: [-5, 256]
    assert isinstance(i,   int)
    assert isinstance(int, type)

def test3 () -> None :
    f: float = float()
    g: float = 0.0
    assert f is not g               # floats are stored by address
    assert f ==     g
    h: float = 0.0
    assert g is     h               # literal caching
    assert isinstance(f,     float)
    assert isinstance(float, type)

def test4 () -> None :
    c: complex = complex()
    d: complex = 0 + 0j
    assert c is not d                   # complexes are stored by address
    assert c ==     d
    e: complex = 0 + 0j
    assert d is     e                   # literal caching
    assert isinstance(c,       complex)
    assert isinstance(complex, type)

def test5 () -> None :
    s: str = str()               # str is like Java's String
    t: str = ""
    assert s is t                # strs are stored by address, but empty is special
    s = "ab"
    s += "c"
    t = "abc"
    assert s is not t
    assert s == t
    u: str = "abc"
    assert t is u                # literal caching
    assert isinstance(s,   str)
    assert isinstance(str, type)






"""
list
    front-loaded array
    ordered
    mutable in structure and value

    add to the back,          amortized O(1)
    add to the middle,        O(n)
    add to the front,         O(n)
    removing from the back,   O(1)
    removing from the middle, O(n)
    removing from the front,  O(n)
    indexing, O(1)
"""
