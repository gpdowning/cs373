# -----------
# Mon, 13 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 19 Jan: Blog  #1
    Sun, 19 Jan: Paper #1: Syllabus

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 13 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Fri, 17 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
"""





"""
class website
    https://www.cs.utexas.edu/users/downing/cs373/

Canvas
    grades
    please check your grades regularly
    Lectures Online
    quizzes

CATME
    team formation
    peer reviews

Ed Discussion
    threaded questions
    private posts to staff
    please be proactive

Ed Lessons
    exercises

GitLab
    class notes
    code examples
    exercise solutions
    project skeletons

HackerRank
    first project

Perusall
    papers

Slack
    group communication
"""





"""
projects
    5, 0-2
    every three weeks, on Wed
    first one is individual
    first one is semi auto-graded on HackerRank
    HackerRank only grades the correctness of the code
    documentation, testing, coverage, continuous integration, issue tracker
    remaining 4 are 4 phases of a Web project
        start thinking about an underserved community
    a TA will mentor 6 of the 24 groups
    each group will have a channel on Slack
    meeting on Zoom weekly with your mentor TA
    provide weekly summary of group progress to your mentor TA
    late up to 2 days, twice
    no make-ups
    resubmits within a week, but not the last
    no 3s

exercises
    12, 0-3
    ~20 min
    auto-graded on Ed Lessons
    have TA review your code before you submit
    no lates
    make-ups within 2 days, twice, but not the last
    no re-submits
    2 3s will make up 1

blogs
    14, 0-2
    prompt posted on Fri, due on Sun
    on any platform, most use Medium
    confirm the word count, confirm that you responded to the prompts
    late up to 2 days, twice
    no make-ups
    no re-submits
    no 3s

papers
    14, 0-3
    posted on Mon, due on Sun
    auto-graded on Perusall
    monitor the annotations, answer each others questions
    late up to 2 days, twice in the term
    no make-ups
    no re-submits
    2 3s will make up 1

quizzes
    42, 0-3
    auto-graded on Canvas
    2 min after the hour
    3 min
    no lates
    make-ups within 2 days, five times, but not the last
    no re-submits
    2 3s will make up 1
"""





"""
ice breaker
    name
    contact info
    where did you grow up
    where did you go to high school
    what's your favorite programming language and why
    what led you to major in CS, when did you decide
    google "cs 373 fall 2024 final entry", click images
"""





"""
UTPC
    competitive programming

    local contests
    regional contests
    world contests

    2014: 2nd, Rice, 1st, Thailand
    2015: 2nd, Rice, 1st, Morocco, our 4 teams were in the top 7
    2016: 1st, 2nd, 3rd, 4th, South Dakota
    2017: Beijing
    2018: Portugal
    2020: Moscow, online, a year late
    2021: didn't make it, Dhaka, a year late
    2023: Luxor, Egypt, a year late
    2023: didn't make it, Luxor, Egypt, a year late
    2024: Astana, Kazakhstan in Sep
    2025: ??? in Sep
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# --------
# Hello.py
# --------

# https://www.python.org
# https://www.python.org/dev/peps/pep-0008/

# https://docs.python.org/3.12/
# https://docs.python.org/3.12/library/

if __name__ == "__main__" :
    print("Nothing to be done.")

""" # pragma: no cover
Developed in 1991 by Guido van Rossum of the Netherlands, now at Dropbox.
Python is procedural, object-oriented, dynamically typed, and garbage collected.

Python 2: 2000
Python 3: 2008



% which python3
/usr/local/bin/python3



% python3 --version
Python 3.13.1



% python3 --help
...



% python3 Hello.py
Nothing to be done.



% chmod ugo+x Hello.py
% ./Hello.py
Nothing to be done.



% python3
Python 3.13.1 (main, Dec  3 2024, 17:59:52) [Clang 16.0.0 (clang-1600.0.26.4)] on darwin
Type "help", "copyright", "credits" or "license" for more information.

>>> import Hello # no print!


>>> quit()



% python3
Python 3.13.1 (main, Dec  3 2024, 17:59:52) [Clang 16.0.0 (clang-1600.0.26.4)] on darwin
Type "help", "copyright", "credits" or "license" for more information.

>>> help()

Welcome to Python 3.12's help utility!

If this is your first time using Python, you should definitely check out
the tutorial on the internet at https://docs.python.org/3.11/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules.  To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, symbols, or topics, type
"modules", "keywords", "symbols", or "topics".  Each module also comes
with a one-line summary of what it does; to list the modules whose name
or summary contain a given string such as "spam", type "modules spam".

help> range
...

help> quit

You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)".  Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.

>>> quit()



% python3
Python 3.13.1 (main, Dec  3 2024, 17:59:52) [Clang 16.0.0 (clang-1600.0.26.4)] on darwin
Type "help", "copyright", "credits" or "license" for more information.

>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!



>>> from __future__ import braces
  File "<stdin>", line 1
SyntaxError: not a chance



>>> import antigravity
...



>>> quit()
"""
