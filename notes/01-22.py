# -----------
# Wed, 22 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 26 Jan: Blog  #2
    Sun, 26 Jan: Paper #2: Makefile

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Fri, 24 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
"""





"""
last time
    finished talking about the grades

this time
    assertions
    unit tests
    coverage
"""





"""
Collatz Conjecture
about 100 years old

take a pos int
if even, divide by 2
otherwise, multiply by 3 and add 1
repeat until 1

5 16 8 4 2 1

the cycle length of  1 is 1
the cycle length of  5 is 6
the cycle length of 10 is 7
"""





/  # true  division, output is always a float
// # floor division, int input, int output; float input, float output





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------------
# Assertions.py
# -------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert_stmt

def cycle_length (n: int) -> int :
    assert n > 0 # precondition
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0 # postcondition
    return c

def test () -> None :
    assert cycle_length( 1) == 1
    assert cycle_length( 5) == 6
    assert cycle_length(10) == 7

if __name__ == "__main__" :
    print("Assertions.py")
    test()
    print("Done.")

""" # pragma: no cover
% python3 Assertions.py
Assertions.py
Traceback (most recent call last):
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/Assertions.py", line 33, in <module>
    test()
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/Assertions.py", line 27, in test
    assert cycle_length( 1) == 1
           ^^^^^^^^^^^^^^^^
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/Assertions.py", line 23, in cycle_length
    assert c > 0
           ^^^^^
AssertionError
"""



"""
Turn OFF assertions with -O
% python3 -O Assertions.py
Assertions.py
Done.
"""





"""
assertions are good for
    preconditions
    postconditions
    loop invariants

not good for
    testing,    instead, unit test framework
    user input, instead, exceptions
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------------
# UnitTests1.py
# -------------

# https://docs.python.org/3/library/unittest.html
# https://docs.python.org/3/library/unittest.html#assert-methods

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 0
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test0 (self) :
        self.assertEqual(cycle_length( 1), 1)

    def test1 (self) :
        self.assertEqual(cycle_length( 5), 6)

    def test2 (self) :
        self.assertEqual(cycle_length(10), 7)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
% ./UnitTests1.py

======================================================================
FAIL: test0 (UnitTests1.MyUnitTests.test0)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/UnitTests1.py", line 31, in test0
    self.assertEqual(cycle_length( 1), 1)
                     ^^^^^^^^^^^^^^^^
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/UnitTests1.py", line 26, in cycle_length
    assert c > 0
           ^^^^^
AssertionError

======================================================================
FAIL: test1 (UnitTests1.MyUnitTests.test1)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/UnitTests1.py", line 34, in test1
    self.assertEqual(cycle_length( 5), 6)
AssertionError: 5 != 6

======================================================================
FAIL: test2 (UnitTests1.MyUnitTests.test2)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/downing/Library/CloudStorage/Dropbox/examples/python/UnitTests1.py", line 37, in test2
    self.assertEqual(cycle_length(10), 7)
AssertionError: 6 != 7

----------------------------------------------------------------------
Ran 3 tests in 0.001s

FAILED (failures=3)
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------------
# UnitTests2.py
# -------------

# https://docs.python.org/3/library/unittest.html
# https://docs.python.org/3/library/unittest.html#assert-methods

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test0 (self) :
        self.assertEqual(cycle_length( 1), 1)

    def test1 (self) :
        self.assertEqual(cycle_length( 5), 5)

    def test2 (self) :
        self.assertEqual(cycle_length(10), 7)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
% ./UnitTests2.py

======================================================================
FAIL: test1 (__main__.MyUnitTests)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/downing/Dropbox/examples/python/./UnitTests2.py", line 33, in test1
    self.assertEqual(cycle_length( 5), 5)
AssertionError: 6 != 5

----------------------------------------------------------------------
Ran 3 tests in 0.000s

FAILED (failures=1)
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# -------------
# UnitTests3.py
# -------------

# https://docs.python.org/3/library/unittest.html
# https://docs.python.org/3/library/unittest.html#assert-methods

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test0 (self) :
        self.assertEqual(cycle_length( 1), 1)

    def test1 (self) :
        self.assertEqual(cycle_length( 5), 6)

    def test2 (self) :
        self.assertEqual(cycle_length(10), 7)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
# -v: verbose
% ./UnitTests3.py -v
test0 (__main__.MyUnitTests.test0) ... ok
test1 (__main__.MyUnitTests.test1) ... ok
test2 (__main__.MyUnitTests.test2) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK


# -m: module
# -v: verbose
% python3 -m unittest UnitTests3 -v
test0 (__main__.MyUnitTests.test0) ... ok
test1 (__main__.MyUnitTests.test1) ... ok
test2 (__main__.MyUnitTests.test2) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK



# -m: module
# -v: verbose
% python3 -m unittest UnitTests3.MyUnitTests -v
test0 (__main__.MyUnitTests.test0) ... ok
test1 (__main__.MyUnitTests.test1) ... ok
test2 (__main__.MyUnitTests.test2) ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK



# -m: module
# -v: verbose
% python3 -m unittest UnitTests3.MyUnitTests.test0 -v
test0 (UnitTests3.MyUnitTests.test0) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ------------
# Coverage1.py
# ------------

# https://coverage.readthedocs.org

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test (self) :
        self.assertEqual(cycle_length(1), 1)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
% which coverage
/usr/local/bin/coverage



% coverage --version
Coverage.py, version 7.2.5 with C extension
Full documentation is at https://coverage.readthedocs.io/en/7.2.5



% coverage help
Coverage.py, version 7.2.5 with C extension
Measure, collect, and report on code coverage in Python programs.

usage: coverage <command> [options] [args]

Commands:
    annotate    Annotate source files with execution information.
    combine     Combine a number of data files.
    debug       Display information about the internals of coverage.py
    erase       Erase previously collected coverage data.
    help        Get help on using coverage.py.
    html        Create an HTML report.
    json        Create a JSON report of coverage results.
    lcov        Create an LCOV report of coverage results.
    report      Report coverage stats on modules.
    run         Run a Python program and measure code execution.
    xml         Create an XML report of coverage results.

Use "coverage help <command>" for detailed help on any command.
Full documentation is at https://coverage.readthedocs.io/en/7.2.5



% python3 -m coverage run Coverage1.py
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK



% ls -al .coverage
-rw-r--r--@ 1 downing  staff  53248 Jan 27 14:25 .coverage



% python3 -m coverage report
Name           Stmts   Miss Branch BrPart  Cover   Missing
----------------------------------------------------------
Coverage1.py      14      4      4      1    61%   19-23
----------------------------------------------------------
TOTAL             14      4      4      1    61%
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ------------
# Coverage2.py
# ------------

# https://coverage.readthedocs.org

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test (self) :
        self.assertEqual(cycle_length(2), 2)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
% python3 -m coverage run Coverage2.py
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK



% python3 -m  coverage report
Name           Stmts   Miss Branch BrPart  Cover   Missing
----------------------------------------------------------
Coverage2.py      14      1      4      1    89%   22
----------------------------------------------------------
TOTAL             14      1      4      1    89%
"""





#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# ------------
# Coverage3.py
# ------------

# https://coverage.readthedocs.org

import unittest # main, TestCase

def cycle_length (n: int) -> int :
    assert n > 0
    c: int = 1
    while n > 1 :
        if (n % 2) == 0 :
            n //= 2
        else :
            n *= 3
            n += 1
        c += 1
    assert c > 0
    return c

class MyUnitTests (unittest.TestCase) :
    def test (self) :
        self.assertEqual(cycle_length(3), 8)

if __name__ == "__main__" :
    unittest.main()

""" # pragma: no cover
% python3 -m coverage run Coverage3.py
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK



% python3 -m coverage report
Name           Stmts   Miss Branch BrPart  Cover   Missing
----------------------------------------------------------
Coverage3.py      14      0      4      0   100%
----------------------------------------------------------
TOTAL             14      0      4      0   100%
"""
