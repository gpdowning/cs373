# -----------
# Fri, 21 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 16 Feb: Blog  #5
    Sun, 16 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 21 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 24 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    comprehensions
    functions

this time
    functions
    decorators
"""





# ------------
# Functions.py
# ------------

def square_def (v) :
    return v ** 2

def test1 () :
    a = [2, 3, 4]
    assert hasattr(square_def, "__call__")
    m = map(square_def, a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





square_lambda = lambda v : v ** 2

def test2 () :
    a = [2, 3, 4]
    assert hasattr(square_lambda, "__call__")
    m = map(square_lambda, a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





class square_class :
    def __call__ (self, v) :
        return v ** 2

def test3 () :
    a = [2, 3, 4]
    assert hasattr(square_class,   "__call__")
    assert hasattr(square_class(), "__call__")
    m = map(square_class(), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def pow_def (p) :
    def f (v) :       # closure
        return v ** p
    return f

f = pow_def(2)
print(f(3))    # 9

def test4 () :
    a = [2, 3, 4]
    assert pow_def(2)(2) == 4
    m = map(pow_def(2), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def pow_lambda (p) :
    return lambda v : v ** p # closure

def test5 () :
    a = [2, 3, 4]
    assert pow_lambda(2)(2) == 4
    m = map(pow_lambda(2), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





class pow_class :
    def __init__ (self, p) :
        self.p = p

    def __call__ (self, v) :
        return v ** self.p

def test6 () :
    a = [2, 3, 4]
    assert pow_class(2)(2) == 4
    m = map(pow_class(2), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





def test7 () :
    a     = [2, 3, 4]
    n     = [1]
    m     = map(lambda v : v ** n[0], a)
    a    += [5]
    n[0]  = 2
    assert list(m) == [4, 9, 16, 25]
    assert not list(m)

def test8 () :
    a = [2, 3, 4]
    n = [1]
    m = map(lambda v : v ** n[0], a)
    a = [2, 3, 4, 5]
    n = [2]
    assert list(m) == [4, 9, 16]
    assert not list(m)

def test9 () :
    a = [2, 3, 4]
    f = [lambda v : v ** n for n in range(3)]
    p = iter(f)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)

def test10 () :
    a = [2, 3, 4]
    f = (lambda v : v ** n for n in range(3))
    p = iter(f)
    m = map(next(p), a)
    assert list(m) == [1, 1, 1]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [2, 3, 4]
    assert not list(m)
    m = map(next(p), a)
    assert list(m) == [4, 9, 16]
    assert not list(m)





# -------------
# Decorators.py
# -------------

import types

def f1 (n) :
    return n + 1

def test1 () :
    assert f1(2) == 3

assert isinstance(f1, types.FunctionType)
assert hasattr(f1, "__call__")

def debug_function (f) :
    assert hasattr(f, "__call__")
    def g (n) :                              # must be a closure that captures f
        print(f.__name__, ":", end=" ")
        print("input =", n, ";", end=" ")
        m = f(n)
        print("output =", m, ";")
        return m
    assert isinstance(g, types.FunctionType)
    assert hasattr(g, "__call__")
    return g

assert isinstance(debug_function, types.FunctionType)
assert hasattr(debug_function, "__call__")

f1 = debug_function(f1)
assert isinstance(f1, types.FunctionType)
assert hasattr(f1, "__call__")

def test2 () :
    assert f1(2) == 3 # f1 : input = 2 ; output = 3 ;

@debug_function
def f2 (n) :
    return n + 1

"""
1. define f2
2. call debug_function on f2
3. put the result back into f2
"""

assert isinstance(f2, types.FunctionType)
assert hasattr(f2, "__call__")

def test3 () :
    assert f2(2) == 3 # f2 : input = 2 ; output = 3 ;





def f4 (n) :
    return n + 1

assert isinstance(f4, types.FunctionType)
assert hasattr(f4, "__call__")

def test4 () :
    assert f4(2) == 3

class debug_class :
    def __init__ (self, f) :
        assert hasattr(f, "__call__")
        self.f = f

    def __call__ (self, n) :
        print(self.f.__name__, ":", end=" ")
        print("input =", n, ";", end=" ")
        m = self.f(n)
        print("output =", m, ";")
        return m

assert not isinstance(debug_class, types.FunctionType)
assert hasattr(debug_class, "__call__")

f4 = debug_class(f4) # invoking constructor
assert not isinstance(f4, types.FunctionType)
assert isinstance(f4, debug_class)
assert hasattr(f4, "__call__")

def test5 () :
    assert f4(2) == 3 # f4 : input = 2 ; output = 3 ;

@debug_class
def f5 (n) :
    return n + 1

assert not isinstance(f5, types.FunctionType)
assert isinstance(f5, debug_class)
assert hasattr(f5, "__call__")

def test6 () :
    assert f5(2) == 3 # f5 : input = 2 ; output = 3 ;
