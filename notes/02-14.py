# -----------
# Fri, 14 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 16 Feb: Blog  #5
    Sun, 16 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 14 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 17 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    Exercise #4: range
    types

this time
    iteration
    types
    yield
"""





# ------------
# Iteration.py
# ------------

def test18 () -> None :
    c: itertools.count = itertools.count() # 0, 1, 2, ...
    assert isinstance(c, itertools.count)
    assert     hasattr(c, "__next__")
    assert     hasattr(c, "__iter__")
    assert not hasattr(c, "__getitem__")
    assert not hasattr(c, "__len__")
    assert iter(c) is c
    s: int = 0
    v: int
    for v in c :
        if v > 4 :
            break
        s += v
    assert s == 10                         # 0+1+2+3+4 = 10
    s = 0
    for v in c :
        if v > 9 :
            break
        s += v
    assert s == 30                         # 6+7+8+9 = 30

def test19 () -> None :
    c: itertools.count = itertools.count(3, 2) # 3, 5, 7, 9, ...
    s: int = 0
    v: int
    for v in c :
        if v > 7 :
            break
        s += v                                 # 3+5+7 = 15
    assert s == 15

def test20 () -> None :
    c: itertools.count = itertools.count(3, -2) # 3, 1, -1, -3, ...
    s: int = 0
    v: int
    for v in c :
        if v < -1 :
            break
        s += v
    assert s == 3                               # 3+1-1 = 3

def test21 () -> None :
    z: zip = zip([2, 3], (4, 5, 6))
    assert isinstance(z, zip)
    assert     hasattr(z, "__next__")
    assert     hasattr(z, "__iter__")
    assert not hasattr(z, "__getitem__")
    assert not hasattr(z, "__len__")
    assert iter(z) is z
    assert list(z) == [(2, 4), (3, 5)]
    assert not list(z)

def test22 () -> None :
    z: zip = zip([2, 3], itertools.count())
    assert list(z) == [(2, 0), (3, 1)]
    assert not list(z)





# --------
# Types.py
# --------

def test6 () -> None :
    a: list = list()              # list is like Java's ArrayList
    b: list = []
    assert a is not b             # lists are stored by address
    assert a ==     b
    a = [2, "abc", 3.45]          # lists are heterogeneous
    b = [2, "abc", 3.45]
    assert a is not b             # no literal caching
    assert a ==     b
    assert a == [2, "abc", 3.45]
    assert a != ["abc", 2, 3.45]  # order matters
    assert isinstance(a,    list)
    assert isinstance(list, type)

def test7 () -> None :
    u: tuple = tuple()              # tuples are like lists, but immutable in size and value
    v: tuple = ()
    assert u is v                   # tuples are stored by address, but empty is special
    u = tuple([2, "abc", 3.45])
    v = (2, "abc", 3.45)
    assert u is not v
    w: tuple = (2, "abc", 3.45)
    assert v is w                   # literal caching
    assert u == (2, "abc", 3.45)
    assert u != ("abc", 2, 3.45)    # order matters
    assert isinstance(u,     tuple)
    assert isinstance(tuple, type)

def test8 () -> None :
    x: set = set()                  # sets are like Java's HashSet
    y: set = set()                  # sets have immutable values
    assert x is not y               # sets are stored by address
    assert x ==     y
    x = {2, "abc", 3.45}
    y = {2, "abc", 3.45}
    assert x is not y               # no literal caching
    assert x ==     y
    x = {2, 2, "abc", 3.45}
    assert x == {2, "abc", 3.45}    # sets have unique values
    assert x == {"abc", 2, 3.45}    # order does not matter
    assert isinstance(x,   set)
    assert isinstance(set, type)

def test9 () -> None :
    x: frozenset = frozenset()              # frozensets are like sets, but immutable in size
    y: frozenset = frozenset()
    assert x is not y                       # frozensets are stored by address
    assert x ==     y
    x = frozenset((2, 2, "abc", 3.45))
    assert x == frozenset([2, "abc", 3.45]) # frozensets have unique values
    assert x == frozenset(["abc", 2, 3.45]) # order does not matter
    assert isinstance(y,         frozenset)
    assert isinstance(frozenset, type)

def test10 () -> None :
    d: dict = dict()                             # dicts are like Java's HashMap
    e: dict = {}                                 # dicts have immutable keys
    assert d is not e                            # dicts are stored by address
    assert d ==     e
    d = {2: "abc", 3: "def", 4: "ghi"}
    e = {2: "abc", 3: "def", 4: "ghi"}
    assert d is not e                            # no literal caching
    assert d ==     e
    d = {2: "xxx", 2: "abc", 3: "def", 4: "ghi"}
    assert d == {2: "abc", 3: "def", 4: "ghi"}   # dicts have unique keys
    assert d == {3: "def", 2: "abc", 4: "ghi"}   # order does not matter
    assert isinstance(d,    dict)
    assert isinstance(dict, type)

def test11 () -> None :
    q: collections.deque = collections.deque()              # deques are like Java's LinkedList
    r: collections.deque = collections.deque()
    assert q is not r                                       # deques are stored by address
    assert q ==     r
    q = collections.deque((2, "abc", 3.45))
    assert q == collections.deque((2, "abc", 3.45))
    assert q != collections.deque(("abc", 2, 3.45))         # order matters
    assert isinstance(q,                 collections.deque)
    assert isinstance(collections.deque, type)

def test12 () -> None :
    def f (v) :
        return v + 1
    assert isinstance(f,                  types.FunctionType)
    assert isinstance(types.FunctionType, type)

def test13 () -> None :
    f: Callable[[Any], Any] = lambda v : v + 1
    assert isinstance(f,                  types.FunctionType)
    assert isinstance(types.FunctionType, type)

def test14 () -> None :
    class A :
        def __init__ (self, i, s, f) :
            self.i = i
            self.s = s
            self.f = f

    x: A = A(2, "abc", 3.45)
    y: A = A(2, "abc", 3.45)
    assert x is not y               # objects are stored by addresss
    assert x !=     y
    assert isinstance(x,    A)
    assert isinstance(x,    object)
    assert isinstance(A,    type)
    assert isinstance(type, type)

    assert issubclass(A, A)
    assert issubclass(A, object)

    assert issubclass(type, type)
    assert issubclass(type, object)

    assert issubclass(object, object)

def test15 () -> None :
    class A :
        def __init__ (self, i, s, f) :
            self.i = i
            self.s = s
            self.f = f

        def __eq__ (self, rhs) :
            return (self.i == rhs.i) and (self.s == rhs.s) and (self.f == rhs.f)

    x: A = A(2, "abc", 3.45)
    y: A = A(2, "abc", 3.45)
    assert x is not y
    assert x ==     y





def f () :
    print("abc")
    yield 2
    print("def")
    yield 3
    print("ghi")

x = f()             # <prints nothing>
print(iter(x) is x) # true

v = next(x) # abc
print(v)    # 2

v = next(x) # def
print(v)    # 3

v = next(x) # ghi, raise StopIteration





# --------
# Yield.py
# --------

def f () :
    yield 2
    yield 3
    yield 4

def test1 () :
    p = f()
    assert isinstance(p, types.GeneratorType)
    assert     hasattr(p, "__next__")
    assert     hasattr(p, "__iter__")
    assert not hasattr(p, "__getitem__")
    assert not hasattr(p, "__len__")
    assert iter(p) is p
    n = next(p)
    assert n == 2
    n = next(p)
    assert n == 3
    n = next(p)
    assert n == 4
    try :
        n = next(p)
    except StopIteration :
        pass

def test2 () :
    p = f()
    assert list(p) == [2, 3, 4]
    assert not list(p)

def g () :
    for v in [2, 3, 4] :
        yield v

def test3 () :
    p = g()
    assert iter(p) is p
    n = next(p)
    assert n == 2
    n = next(p)
    assert n == 3
    n = next(p)
    assert n == 4
    try :
        n = next(p)
    except StopIteration :
        pass

def test4 () :
    p = g()
    assert list(p) == [2, 3, 4]
    assert not list(p)





class my_range () :
    class iterator () :
        def __init__ (self, start, stop) :
            self.start = start
            self.stop  = stop

        def __iter__ (self) :
            return self

        def __next__ (self) :
            if self.start >= self.stop :
                raise StopIteration()
            start = self.start
            start += 1
            return start

    def __init__ (self, start, stop) :
        self.start = start
        self.stop  = stop

    def __iter__ (self) :
#       return my_range.iterator(self.start, self.stop)
        start = self.start
        while start < self.stop :
            yield start
            start += 1
