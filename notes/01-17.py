# -----------
# Fri, 17 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 19 Jan: Blog  #1
    Sun, 19 Jan: Paper #1: Syllabus

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 17 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
"""





"""
run the code as is, confirm success
add tests
run the code again, confirm failure
fix the code
run the code again, confirm success
"""





"""
if you have bug

1. create an issue
2. write a test that fails
3. fix the code
4. mark the issue resolved
"""





"""
1. install tools

2. ssh into a CS machine

3. Docker
"""





FROM python

# update Linux package manager
RUN apt-get update

# update Python package manager
RUN pip install --upgrade pip
RUN pip --version

# auto formatter
RUN pip install black

# coverage tool
RUN pip install coverage

# line-ending converter
RUN apt-get -y install dos2unix

# type annotator
RUN pip install mypy

# static analyzer
RUN pip install pylint

# editor
RUN apt-get -y install vim

# Boost library, required by checktestdata
RUN apt-get -y install libboost-dev

# GNU Bignum library, required by checktestdata
RUN apt-get -y install libgmp-dev

# build checktestdata, an input verifier
RUN git clone https://github.com/DOMjudge/checktestdata checktestdata && \
    cd checktestdata                                                  && \
    git checkout release                                              && \
    ./bootstrap                                                       && \
    make                                                              && \
    cp checktestdata /usr/bin                                         && \
    cd -

CMD bash





# ---------------------
# Docker-Push.worksheet
# ---------------------

# https://www.docker.com
# https://docs.docker.com/engine/reference/run/



% which docker
/usr/local/bin/docker



% docker --version
Docker version 27.5.0, build a187fa5d2d



% docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE



# -t: Name and optionally a tag in the 'name:tag' format
% docker build -t gpdowning/python .
...



% docker images
REPOSITORY      TAG       IMAGE ID       CREATED         SIZE
gpdowning/python    latest    bac63dd19cf1   8 minutes ago    1.35GB



% docker login -u gpdowning
Password: <access token>
...



% docker push gpdowning/python
...



# https://hub.docker.com/r/gpdowning/python/





# ---------------------
# Docker-Pull.worksheet
# ---------------------

# https://www.docker.com
# https://docs.docker.com/engine/reference/run/



% which docker
/usr/local/bin/docker



% docker --version
Docker version 27.5.0, build a187fa5d2d



% docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE



% docker pull gpdowning/python
...



% docker images
REPOSITORY      TAG       IMAGE ID       CREATED          SIZE
gpdowning/python    latest    bac63dd19cf1   8 minutes ago    1.35GB



% pwd
/Users/downing/git/cs371p/python



% ls
Docker-Pull.worksheet   Docker-Push.worksheet   Dockerfile      Makefile



# --rm: Automatically remove the container when it exits
# -i:   Keep STDIN open even if not attached
# -t:   Allocate a pseudo-TTY
# -v:   Bind mount a volume
# -w:   Working directory inside the container
% docker run --rm -i -t -v /Users/downing/git/cs371p/python:/usr/python -w /usr/python gpdowning/python
/usr/python# pwd
/usr/python



/usr/python# ls
Docker-Pull.worksheet   Docker-Push.worksheet   Dockerfile      Makefile





# -------------------------
# Docker-Versions.worksheet
# -------------------------

# https://www.docker.com
# https://docs.docker.com/engine/reference/run/



/usr/python# which black
/usr/local/bin/black

/usr/python# black --version
black, 24.10.0 (compiled: yes)



/usr/python# which checktestdata
/usr/bin/checktestdata

/usr/python# checktestdata --version
checktestdata -- version 20250117, written by Jan Kuipers, Jaap Eldering, Tobias Werth



/usr/python# which coverage
/usr/local/bin/coverage

/usr/python# coverage --version | head -n 1
Coverage.py, version 7.6.10 with C extension



/usr/python# which mypy
/usr/local/bin/mypy

/usr/python# mypy --version
mypy 1.14.1 (compiled: yes)




/usr/python# which pip
/usr/local/bin/pip

/usr/python# pip --version
pip 24.3.1 from /usr/local/lib/python3.13/site-packages/pip (python 3.13)



/usr/python# which pylint
/usr/local/bin/pylint

/usr/python# pylint --version | head -n 1
pylint 3.3.3




/usr/python# which python3
/usr/local/bin/python3

/usr/python# python3 --version
Python 3.13.1



/usr/python# which vim
/usr/bin/vim

/usr/python# vim --version | head -n 1
VIM - Vi IMproved 9.0 (2022 Jun 28, compiled May 04 2023 10:24:44)





# ---------------------
# CS-Versions.worksheet
# ---------------------

# https://www.docker.com
# https://docs.docker.com/engine/reference/run/



cs:~% which astyle
/lusr/bin/astyle

cs:~% astyle --version
Artistic Style Version 3.1



cs:~% which checktestdata
/lusr/bin/checktestdata

cs:~% checktestdata --version
checktestdata -- version 20181113, written by Jan Kuipers, Jaap Eldering, Tobias Werth



cs:~% which cppcheck
/lusr/bin/cppcheck

cs:~% cppcheck --version
Cppcheck 2.11



cs:~% which doxygen
/lusr/bin/doxygen
cs:~% doxygen --version
1.9.8



# which g++-10
/usr/bin/g++-10

cs:~% g++-10 --version | head -n 1
g++-10 (Ubuntu 10.5.0-1ubuntu1~20.04) 10.5.0



cs:~% which valgrind
/usr/bin/valgrind

cs:~% valgrind --version
valgrind-3.15.0



cs:~% which vim
/usr/bin/vim

cs:~% vim --version | head -n 1
VIM - Vi IMproved 8.1 (2018 May 18, compiled Nov 21 2024 18:12:44)



cs:~% grep "#define BOOST_LIB_VERSION " /usr/include/boost/version.hpp
#define BOOST_LIB_VERSION "1_71"



cs:~% ls -al /usr/include/gtest/gtest.h
-rw-r--r-- 1 root root 93924 Jan 25  2020 /usr/include/gtest/gtest.h



cs:~% pkg-config --modversion gtest
1.10.0



cs:~% /usr/lib/x86_64-linux-gnu/libgtest*
-rw-r--r-- 1 root root 2526200 Jan 17 00:36 /usr/lib/libgtest.a
-rw-r--r-- 1 root root    3212 Jan 17 00:36 /usr/lib/libgtest_main.a
