# -----------
# Mon, 10 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 16 Feb: Blog  #5
    Sun, 16 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 10 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Fri, 14 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/
"""

"""
last time
    operators

this time
    operators
    iteration
    range
"""





# ------------
# Operators.py
# ------------

def test48 () :
    i = 3
    j = 5
    k = 7
    l = 8
    assert (i < j) and (j < k) and (k < l)
    assert i < j < k < l

def test49 () :
    class A :
        k = 4 # static in Java

        def __init__ (self, i, j) :
            self.i = i # non-static in Java
            self.j = j

    assert A.k == 4
    x = A(2, 3)
    assert x.i == 2
    assert x.j == 3
    assert x.k == 3 # bad idea





# ------------
# Iteration.py
# ------------

def test1 () -> None :
    a: list[int] = [2, 3, 4]
    assert isinstance(a, list)
    assert not hasattr(a, "__next__")
    assert     hasattr(a, "__iter__")
    assert     hasattr(a, "__getitem__")
    assert     hasattr(a, "__len__")
    s: int = 0
    v: int
    for v in a :
        s += v
    assert s == 9

def test2 () -> None :
    a: list[int] = [2, 3, 4]
    v: int
    for v in a :
        v += 1            # ?
    assert a == [2, 3, 4]

def test3 () -> None :
    a: list[list[int]] = [[2], [3], [4]]
    v: list[int]
    for v in a :
        v += (5,)                        # ? # v.__iadd__()
    assert a == [[2, 5], [3, 5], [4, 5]]

def test4 () -> None :
    a: list[tuple[int, ...]] = [(2,), (3,), (4,)]
    v: tuple[int, ...]
    for v in a :
        v += (5,)                  # ? # v = v + (5,); v = v.add()
    assert a == [(2,), (3,), (4,)]

def test5 () -> None :
    a: list[str] = ["abc", "def", "ghi"]
    v: str
    for v in a :
        v += "x"                      # ?
    assert a == ["abc", "def", "ghi"]





for v in a :    # a must be iterable
    ...

for u, v in a : # a must be iterable over iterables of len 2
    ...





def test6 () -> None :
    a: list = [[2, "abc"], (3, "def"), [4, "ghi"]]
    s: int  = 0
    u: int
    for u, _ in a :
        s += u
    assert s == 9





"""
set, like Java's HashSet
    no duplicate keys
    not ordered
    mutable in structure, immutable in keys
"""

def test7 () -> None :
    x: set[int] = {2, 3, 4}
    assert isinstance(x, set)
    assert not hasattr(x, "__next__")
    assert     hasattr(x, "__iter__")
    assert not hasattr(x, "__getitem__")
    assert     hasattr(x, "__len__")
    s: int = 0
    v: int
    for v in x :                         # order IS NOT guaranteed
        s += v
    assert s == 9





"""
dict, like Java's HashMap
    no duplicate keys
    ordered!!!
    mutable in structure, immutable in keys, mutable in values
"""

def test8 () -> None :
    d: dict[int, str] = {2: "abc", 3: "def", 4: "ghi"}
    assert isinstance(d, dict)
    assert not hasattr(d, "__next__")
    assert     hasattr(d, "__iter__")
    assert     hasattr(d, "__getitem__") # with the keys
    assert     hasattr(d, "__len__")
    s: int = 0
    k: int
    for k in d :                         # order IS guaranteed
        s += k
    assert s == 9





def test9 () -> None :
    d: dict[int, str]            = {2: "abc", 3: "def"}
    k1: collections.abc.KeysView = d.keys()             # O(1)
    assert str(type(k1)) == "<class 'dict_keys'>"
    assert not hasattr(k1, "__next__")
    assert     hasattr(k1, "__iter__")
    assert not hasattr(k1, "__getitem__")
    assert     hasattr(k1, "__len__")
    assert list(k1) == [2, 3] # O(n)
    k2: collections.abc.KeysView = d.keys()
    assert k1 is not k2
    d[4] = "ghi"
    assert d == {2: "abc", 3: "def", 4: "ghi"}
    assert list(k1) == [2, 3, 4]
    assert list(k2) == [2, 3, 4]

def test10 () -> None :
    d: dict[int, str]             = {2: "abc", 3: "def", 4: "ghi"}
    v: collections.abc.ValuesView = d.values()
    assert str(type(v)) == "<class 'dict_values'>"
    assert list(v)      == ["abc", "def", "ghi"]

def test11 () -> None :
    d:  dict[int, str]            = {2: "abc", 3: "def", 4: "ghi"}
    kv: collections.abc.ItemsView = d.items()
    assert str(type(kv)) == "<class 'dict_items'>"
    assert list(kv)      == [(2, "abc"), (3, "def"), (4, "ghi")]

def test12 () -> None :
    r: range = range(10)
    assert isinstance(r, range)
    assert not hasattr(r, "__next__")
    assert     hasattr(r, "__iter__")
    assert     hasattr(r, "__getitem__")
    assert     hasattr(r, "__len__")
    assert list(r) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert list(r) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

def test13 () -> None :
    r: range = range(2, 10)
    assert list(r) == [2, 3, 4, 5, 6, 7, 8, 9]

def test14 () -> None :
    r: range = range(2, 10, 2)
    assert list(r) == [2, 4, 6, 8]

def test15 () -> None :
    r: range = range(10, 2, -2)
    assert list(r) == [10, 8, 6, 4]

def test16 () -> None :
    r: range = range(10)
    assert r[0] == 0
    assert r[9] == 9
    try :
        assert r[10] == 10 # error: out of range
        assert False
    except IndexError :
        pass
    #r[0] = 2              # TypeError: 'range' object does not support item assignment

def test17 () -> None :
    r: range = range(15)
    s: int   = 0
    v: int
    for v in r :
        if v == 10 :
            break
        s += v
    else :           # else clause in a for loop
        assert False # executes when the loop terminates normally
    assert s == 45





x = range(...)
print(type(x)) # range

p = iter(x)    # x.__iter__()
print(type(p)) # range.iterator

q = iter(p)
print(q is p)  # true!!!





class range :
    class iterator :
        def __init__ (self, start = 0, stop, step = 1) :
            ...

        def __iter__ (self) :
            return self

        def __next__ (self) :
            ...

    def __init__ (self, start = 0, stop, step = 1) :
        ...

    def __getitem__ (self, index) :
        ...

    def __iter__ (self) :
        return range.iterator(...)
