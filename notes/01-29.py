# -----------
# Wed, 29 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Feb: Blog  #3
    Sun, 2 Feb: Paper #3

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/

    Fri, 31 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
"""

"""
last time
    exceptions

this time
    recursion and iteration
    Exercise #2: factorial()
"""

"""
define factorial

recursively
iteratively, with while
iteratively, for and range
"""





# recursive procedure
# recursive process
def factorial_recursion (n: int) -> int :
    assert n >= 0
    if n < 2 :
        return 1
    return n * factorial_recursion(n - 1)

# recursive procedure
# iterative process
def factorial_tail_recursion (n: int, v: int = 1) -> int :
    assert n >= 0
    assert v >= 1
    if n < 2 :
        return v
    return factorial_tail_recursion(n - 1 , n * v)

# iterative procedure
# iterative process
def factorial_while (n: int) -> int :
    assert n >= 0
    v: int = 1
    while n > 1 :
        v *= n
        n -= 1
    return v

# iterative procedure
# iterative process
def factorial_range_for (n: int) -> int :
    assert n >= 0
    v: int = 1
    i: int
    for i in range(1, n + 1) :
        v *= i
    return v





a = [2, 3, 4] # list, indexable
for v in a :
    ...





i = 0
while (i < len(a)) :
    v = a[i]
    ...
    i += 1





# Java

LinkedList x = new LinkedList();
Iterator   p = x.iterator();

while (p.hasNext()) {
    ? v = p.next();
    ...}





a = deque([2, 3, 4]) # linked list
for v in a :
    ...





p = iter(a)          # deque iterator
try :
    while (True) :
        v = next(p)
        ...
except StopIteration :
    pass
