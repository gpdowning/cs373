# -----------
# Mon, 24 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Mar: Blog  #7
    Sun, 2 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 24 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/

    Fri, 27 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    functions
    decorators

this time
    Exercise #5: Cache
    decorators
"""





def cache (f) :
    d = {}
    def new_f (n) :
        if n not in d :
            d[n] = f(n)
        return d[n]
    return new_f





class cache :
    def __init__ (self, f) :
        self.d = {}
        self.f = f

    def __call__ (self, n) :
        if n not in self.d :
            self.d[n] = self.f(n)
        return self.d[n]





# -------------
# Decorators.py
# -------------

def repeat_three (f) :
    def g () :
        for _ in range(3) :
            f()
    return g

@repeat_three
def f6 () :
    print("Hi")

def test7 () :
    f6()                        # Hi Hi Hi




def repeat_N (n) :
    def h (f) :
        def g () :
            for _ in range(n) :
                f()
        return g
    return h

@repeat_N(3)
def f7 () :
    print("Hi")

def test8 () :
    f7()                        # Hi Hi Hi
