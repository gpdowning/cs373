# -----------
# Fri, 24 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 26 Jan: Blog  #2
    Sun, 26 Jan: Paper #2: Makefile

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 24 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
"""





"""
1. run the code as is, confirm success
2. identify and fix the broken tests
3. run the code as is, confirm failure
4. identify and fix the broken code
5. run the code as is, confirm success
"""

def is_prime (n: int) -> bool :
    assert n > 0
    if n == 2 :
        return true
    if (n == 1) or ((n % 2) == 0) :
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2) :
        if (n % i) == 0 :
            return False
    return True





"""
last time
    assertions
    unit tests

this time
    exercise
    exceptions
"""





# C does not have exceptions

# Python: use the return

def f (...) :
    ...
    if (<something wrong>)
        return <special value>
    ...

def g (...) :
    ...
    x = f(...)
    if (x == <special value>)
        <something wrong>
    ...





# Python: use a global

h = 0

def f (...) :
    global h
    ...
    if (<something wrong>)
        h = <special value>
        return ...
    ...

def g (...) :
    global h
    ...
    h = 0
    x = f(...)
    if (h == <special value>)
        <something wrong>
    ...





# Python: use an arg, by value, doesn't work

def f (..., e2) :
    ...
    if (<something wrong>)
        e2 = <special value>
        return ...
    ...

def g (...) :
    ...
    e = 0
    x = f(..., e)
    if (e == <special value>)
        <something wrong>
    ...





# Java: use an arg, by value, doesn't work

int f (..., int e2) {
    ...
    if (<something wrong>) {
        e2 = <special value>;
        return ...;}
    ...}

void g (...) {
    ...
    int e = 0;
    int x = f(..., e)
    if (e == <special value>)
        <something wrong>;
    ...}





# Java: us an arg, by reference, immutable, doesn't work

int f (..., Integer e2) {
    ...
    if (<something wrong>) {
        e2 = <special value>;
        return ...;}
    ...}

void g (...) {
    ...
    Integer e = Integer(0);
    int x = f(..., e)
    if (e == <special value>)
        <something wrong>;
    ...}





# Java: us an arg, by reference, mutable, does work

int f (..., int[] e2) {
    ...
    if (<something wrong>) {
        e2[0] = <special value>;
        return ...;}
    ...}

void g (...) {
    ...
    int[] e = {0};
    int x = f(..., e)
    if (e[0] == <special value>)
        <something wrong>;
    ...}





# Python: us an arg, by reference, mutable, does work

def f (..., e2) :
    ...
    if (<something wrong>)
        e2[0] = <special value>
        return ...
    ...

def g (...) :
    ...
    e = [0]
    x = f(..., e)
    if (e[0] == <special value>)
        <something wrong>
    ...




# Python: use an exception

def f (..., e2) :
    ...
    if (<something wrong>)
        raise E
    ...

def g (...) :
    ...
    try :
        x = f(..., e)
    except E :
        <something wrong>
    ...
