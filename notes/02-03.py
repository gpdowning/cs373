# -----------
# Mon,  3 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #4
    Sun, 9 Feb: Paper #4

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 3 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Fri,  7 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/
"""





"""
last time
    factorial()
    reduce()

this time
    unpacking
    Exercise #3: reduce()
"""





def test1 () :
    x, y = 2, 3
    assert x  == 2
    assert y  == 3

def test2 () :
    x, y = 2, 3
    x, y = y, x
    assert x  == 3
    assert y  == 2

def test3 () :
    x, y, z = [2, 3, 4]
    assert x  == 2
    assert y  == 3
    assert z  == 4

def test4 () :
    x, *ys = [2, 3, 4]
    assert x  == 2
    assert ys == [3, 4]

def test5 () :
    *xs, y = [2, 3, 4]
    assert xs == [2, 3]
    assert y  == 4

def test6 () :
    x, *ys, z = [2, 3, 4]
    assert x  == 2
    assert ys == [3]
    assert z  == 4





def reduce (bf, xs, v) :
    for x in xs :
        v = bf(v, x)
    return v

def reduce (bf, xs, v) :
    if not xs :
        return v
    *xs, y = xs
    return bf(my_reduce(bf, xs, v), y)





def reduce (bf, a, v = None) :
    if not xs and v is None :
        raise TypeError
    if v is None :
        v, *xs = xs
    for x in xs :
        v = bf(v, x)
    return v
