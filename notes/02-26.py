# -----------
# Wed, 26 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Mar: Blog  #7
    Sun, 2 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/

    Fri, 27 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Mar, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    Exercise #5: Cache
    decorators

this time
    function defaults
    function keywords
    function unpacking
    function tuple
    function dict
"""





"""
Google Doc rubrics
Ed post
assessments of your developer
    RESTful API
    technical report
    user stories
    website
implement API
two docker images for the frontend and the backend tool chains
MySQL or PostreSQL
UML, class diagram
5 new user stories
code the same between the model pages and the instance page connections
model pages
    pagination
    how many cards are there, how many cards on the page, how many pages are there
    first page, last page, next page, prev page
"""





"""
tokens
    =, *, **

contexts
    function call
    function def
"""





# -------------------
# FunctionDefaults.py
# -------------------

def f (x, y, z) :
    return [x, y, z]

def test1 () :
    # f(2, 3)                      # TypeError: f() missing 1 required positional argument: 'z'
    assert f(2, 3, 4) == [2, 3, 4]
    # f(2, 3, 4, 5)                # TypeError: f() takes 3 positional arguments but 4 were given





def g1 (x, y, z=5) :
    return [x, y, z]

def test2 () :
    assert g1(2, 3)    == [2, 3, 5]
    assert g1(2, 3, 4) == [2, 3, 4]

# def g2 (x, y=5, z) : # SyntaxError: non-default argument follows default argument
#     return [x, y, z]





def h1 (x=[]) : # mutable default
    x += [2]
    return x

def test3 () :
    assert h1()    == [2]
    assert h1()    == [2, 2]
    assert h1([1]) == [1, 2]
    assert h1()    == [2, 2, 2]
    assert h1([1]) == [1, 2]





def h2 (x=()) : # immutable default
    x += (2,)
    return x

def test4 () :
    assert h2()     == (2,)
    assert h2()     == (2,)
    assert h2((1,)) == (1, 2)
    assert h2()     == (2,)
    assert h2((1,)) == (1, 2)





def h3 (x=None) :
    if v
        x = []
    x += [2]
    return x

def test5 () :
    assert h3()     == [2]
    assert h3()     == [2]
    assert h3([1])  == [1, 2]
    assert h3()     == [2]
    assert h3([1])  == [1, 2]
    assert h3(None) == [2]





# -------------------
# FunctionKeywords.py
# -------------------

def f (x, y, z) :
    return [x, y, z]

def test1 () :
    assert f(2, z=4, y=3) == [2, 3, 4]
    # f(z=4, 2,   y=3)                 # SyntaxError: non-keyword arg after keyword arg; position must precede by name
    # f(2,   x=2, y=3)                 # TypeError: f() got multiple values for argument 'x'
    # f(2,   a=4, y=3)                 # TypeError: f() got an unexpected keyword argument 'a'





def g (x, *, y, z) :
    return [x, y, z]

def test2 () :
    # g(2, 3, 4)                         # TypeError: f() takes 1 positional argument but 3 were given
    assert g(2,   z=4, y=3) == [2, 3, 4]
    assert g(x=2, z=4, y=3) == [2, 3, 4]





# --------------------
# FunctionUnpacking.py
# --------------------

def f (x, y, z) :
    return [x, y, z]

def test1 () :
    t = (3, 4)
    assert f(2, 5, t)  == [2, 5, (3, 4)]
    assert f(2,   *t)  == [2, 3, 4]      # order matters
    assert f(*t,  2)   == [3, 4, 2]      # position and iterable unpacking are processed left to right
    assert f(z=2, *t)  == [3, 4, 2]      # unpacking goes first, order doesn't matter
    assert f(*t,  z=2) == [3, 4, 2]
    # f(x=2, *t)                         # TypeError: f() got multiple values for argument 'x'
    # f(*t,  x=2)                        # TypeError: f() got multiple values for argument 'x'

def test2 () :
    u = (2, 3)
    v = (4,)
    assert f(*u, *v)     == [2, 3, 4]
    assert  [*u, *v, *u] == [2, 3, 4, 2, 3]
    assert  (*u, *v, *v) == (2, 3, 4, 4)
    assert  {*u, *v, *u} == {2, 3, 4}

def test3 () :
    a      = [2, 3, 4]
    x, *xs = a
    assert x  == 2
    assert isinstance(xs, list)
    assert xs == [3, 4]
    x, y, *xs = a
    assert x  == 2
    assert y  == 3
    assert xs == [4]
    *xs, x = a
    assert xs == [2, 3]
    assert x  == 4

class MyList :
    def __init__ (self, a) :
        self.a = a

    def __iter__ (self) :
        return iter(self.a)

def test4 () :
    a = [2, 3, 4]
    x = MyList(a)
    assert [*x, *x] == [2, 3, 4, 2, 3, 4]

def test5 () :
    d = {"z" : 4, "y" : 3, "x" : 2}
    assert f(**d) == [2, 3, 4]

def test6 () :
    d = {"z" : 4, "y" : 3}
    assert f(2,   **d) == [2, 3, 4]
    # f(**d, 2)                       # SyntaxError: invalid syntax; position must precede dict unpacking
    assert f(x=2, **d) == [2, 3, 4]   # order doesn't matter
    assert f(**d, x=2) == [2, 3, 4]
    # assert f(z=2, **d) == [2, 3, 4] # TypeError: f() got multiple values for keyword argument 'z'
    # assert f(**d, z=2) == [2, 3, 4] # TypeError: f() got multiple values for keyword argument 'z'

def test7 () :
    t = (2, 3)
    d = {"z" : 4}
    assert f(*t, **d) == [2, 3, 4]
    # f(**d, *t)                      # SyntaxError: iterable argument unpacking follows keyword argument unpacking; iterable unpacking must precede dict unpacking

def test8 () :
    u = {"x":2, "y":3}
    v = {"z":4}
    assert f(**u, **v)       == [2, 3, 4]
    assert  {**u, **v, **u}  == {"x": 2, "y": 3, "z": 4}

class MyDict :
    def __init__ (self, d) :
        self.d = d

    def __getitem__ (self, k) :
        return self.d[k]

    def keys (self) :
        return self.d.keys()

def test9 () :
    d = {"z" : 4, "y" : 3, "x" : 2}
    x = MyDict(d)
    assert {**x, **x} == {"x" : 2, "y" : 3, "z" : 4}





# ----------------
# FunctionTuple.py
# ----------------

def f (x, y, *t) :
    return [x, y, t]

def test1 () :
    assert f(2, 3)       == [2, 3, ()]
    assert f(2, 3, 4)    == [2, 3, (4,)]
    assert f(2, 3, 4, 5) == [2, 3, (4, 5)]
    f(t = ...) # no

def test2 () :
    t = (3, 4)
    assert f(2, 5,  t)  == [2, 5, ((3, 4),)]
    assert f(2, 5, *t)  == [2, 5, (3, 4)]
    assert f(2, *t)     == [2, 3, (4,)]
    assert f(*t)        == [3, 4, ()]





# ---------------
# FunctionDict.py
# ---------------

def f (x, y, **d) :
    return [x, y, d]

def test1 () :
    assert f(2, 3)           == [2, 3, {}]
    assert f(2, 3, a=4)      == [2, 3, {'a': 4}]
    assert f(2, 3, a=4, b=5) == [2, 3, {'a': 4, 'b': 5}]
    f(d = ...) # no

def test2 () :
    d = {"b": 4, "a": 3}
    assert f(2, 5,   **d)  == [2, 5, {'a': 3, 'b': 4}]
    assert f(2, y=5, **d)  == [2, 5, {'a': 3, 'b': 4}]
    u = (2,)
    assert f(y=5, *u,  **d) == [2, 5, {'a': 3, 'b': 4}]
    assert f(*u,  y=5, **d) == [2, 5, {'a': 3, 'b': 4}]

def test3 () :
    d = {"y": 3, "a": 2}
    assert f(2,   **d) == [2, 3, {'a': 2}]
    assert f(x=2, **d) == [2, 3, {'a': 2}]





def f (*t, **d) :
    ...





def h3 (*t, **d) :
    if not t and not d :
        ...
