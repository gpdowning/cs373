# -----------
# Fri, 28 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Mar: Blog  #7
    Sun, 2 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 27 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Mar, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Mar: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #4: WiCS
    https://www.cs.utexas.edu/~utpc/
"""

"""
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
"""





"""
last time
    function defaults
    function keywords
    function unpacking
    function tuple
    function dict

this time
    regular expressions
    with
    instance methods
    static methods
    class methods
"""





# ----------
# RegExps.py
# ----------

def test1 () :
    s = "b ab\naab 123"
    a = re.split("ab", s)
    assert isinstance(a, list)
    assert a == ['b ', '\na', ' 123']

def test2 () :
    s = "b ab\naab 123"
    a = re.split("ba", s)
    assert isinstance(a, list)
    assert a == [s]

def test3 () :
    s = "b ab\naab 123"
    a = re.split("^b", s)            # start of string
    assert isinstance(a, list)
    assert a == ['', ' ab\naab 123']

def test4 () :
    s = "b ab\naab 123"
    a = re.split("^a", s)      # start of string
    assert isinstance(a, list)
    assert a == [s]

def test5 () :
    s = "b ab\naab 123"
    a = re.split("^a", s, flags=re.M) # multiline
    assert isinstance(a, list)
    assert a == ['b ab\n', 'ab 123']

def test6 () :
    s = "b ab\naab 123"
    a = re.split("3$", s)            # end of string
    assert isinstance(a, list)
    assert a == ['b ab\naab 12', '']

def test7 () :
    s = "b ab\naab 123"
    a = re.split("b$", s)      # end of string
    assert isinstance(a, list)
    assert a == [s]

def test8 () :
    s = "b ab\naab 123"
    a = re.split("b$", s, flags=re.M) # multiline
    assert isinstance(a, list)
    assert a == ['b a', '\naab 123']

def test9 () :
    s = "b ab\naab 123"
    a = re.split(".", s)                                           # any character
    assert isinstance(a, list)
    assert a == ['', '', '', '', '\n', '', '', '', '', '', '', '']

def test10 () :
    s = "b ab\naab 123"
    a = re.split(r"\d", s)                 # any digit
    assert isinstance(a, list)
    assert a == ['b ab\naab ', '', '', '']

def test11 () :
    s = "b ab\naab 123"
    a = re.split(r"\D", s)                                  # any non-digit
    assert isinstance(a, list)
    assert a == ['', '', '', '', '', '', '', '', '', '123']

def test12 () :
    s = "b ab\naab 123"
    a = re.split(r"\w", s)                                   # any alphanumeric
    assert isinstance(a, list)
    assert a == ['', ' ', '', '\n', '', '', ' ', '', '', '']

def test13 () :
    s = "b ab\naab 123"
    a = re.split(r"\W", s)                # any non-alphanumeric
    assert isinstance(a, list)
    assert a == ['b', 'ab', 'aab', '123']

def test14 () :
    s = "b ab\naab 123"
    m = re.search("(a*)b([^a]*)(a*)b", s) # * is zero or more
    assert isinstance(m, re.Match)
    assert hasattr(m, "__getitem__")
    assert not hasattr(m, "__iter__")
    assert not hasattr(m, "__next__")
    assert not hasattr(m, "__len__")
    assert m[0] == "b ab"                 # re.Match.__getitem__()
    assert m[1] == ""
    assert m[2] == " "
    assert m[3] == "a"
    try :
        assert m[4]
        assert False
    except IndexError :
        pass

def test15 () :
    s = "b ab\naab 123"
    m = re.search("(a+)b([^a]*)(a+)b", s) # + is one or more
    assert isinstance(m, re.Match)
    assert m[0] == "ab\naab"
    assert m[1] == "a"
    assert m[2] == "\n"
    assert m[3] == "aa"
    try :
        assert m[4]
        assert False
    except IndexError :
        pass

def test16 () :
    s = "b ab\naab 123"
    m = re.search("((a?)b([^a]*))(a?)b", s) # ? is zero or one
    assert isinstance(m, re.Match)
    assert m[0] == "b ab"
    assert m[1] == "b "
    assert m[2] == ""
    assert m[3] == " "
    assert m[4] == "a"

def test17 () :
    s = "b ab\naab 123"
    t = re.sub("b ", "xx", s)
    assert s == "b ab\naab 123"
    assert t == "xxab\naaxx123"

def test18 () :
    s = "b ab\naab 123"
    t = re.sub("b.", "xx", s)
    assert s == "b ab\naab 123"
    assert t == "xxab\naaxx123"

def test19 () :
    s = "b ab\naab 123"
    t = re.sub("", "z", s)
    assert s == "b ab\naab 123"
    assert t == "zbz zazbz\nzazazbz z1z2z3z"





# -------
# With.py
# -------

def test1 () :
    print("test1:")
    print("opening file")
    f = open("With.py")
    try :
        assert f.readline() == "#!/usr/bin/env python3\n"
    finally :
        print("closing file")
        f.close()
    print()

def test2 () :
    print("test2:")
    with open("With.py") as f :
        assert f.readline() == "#!/usr/bin/env python3\n"
    print()

class my_open :
    def __init__ (self, s) :
        self.s = s
        self.f = None

    def __enter__ (self) :
        print("opening file")
        self.f = open(self.s)
        return self.f

    def __exit__ (self, *t) :
        print("closing file")
        self.f.close()

def test3 () :
    print("test3:")
    x = my_open("With.py")
    f = x.__enter__()
    try :
        assert f.readline() == "#!/usr/bin/env python3\n"
    finally :
        x.__exit__()
    print()

def test4 () :
    print("test4:")
    with my_open("With.py") as f :
        assert f.readline() == "#!/usr/bin/env python3\n"

"""
% With.py
With.py
test1:
opening file
closing file

test2:

test3:
opening file
closing file

test4:
opening file
closing file
"""





# ------------------
# InstanceMethods.py
# ------------------

class A :
    cv = 0

    def __init__ (self) :
        # cv    += 1 # UnboundLocalError: cannot access local variable 'cv' where it is not associated with a value
        A.cv    += 1
        self.cv += 0 # misleading

        # iv
        # A.iv  = 1  # AttributeError: type object 'A' has no attribute 'iv'. Did you mean: 'cv'?
        self.iv = 1

    def im (self) :
        A.cv    += 1
        self.iv += 1

        # sm()         # NameError: name 'sm' is not defined
        A.sm()
        self.sm()      # misleading

        # cm()         # NameError: name 'cm' is not defined
        A.cm()
        self.cm()      # misleading

    @classmethod
    def cm (cls) :
        pass

    @staticmethod
    def sm () :
        pass

def test1 () :
    # assert cv   == 0 # NameError: name 'cv' is not defined
    assert A.cv == 0

    x = A()
    assert A.cv == 1
    assert x.cv == 1 # misleading

def test2 () :
    # assert iv   == 0 # NameError: name 'iv' is not defined
    # assert A.iv == 1 # AttributeError: type object 'A' has no attribute 'iv'

    x = A()
    assert A.cv == 2
    assert x.iv == 1

def test3 () :
    # im()           # NameError: name 'im' is not defined
    # A.im()         # TypeError: im() missing 1 required positional argument: 'self'

    x = A()
    assert A.cv == 3
    assert x.iv == 1

    x.im()
    assert A.cv == 4
    assert x.iv == 2

    A.im(x)          # methods are really just functions
    assert A.cv == 5
    assert x.iv == 3

def test4 () :
    m = map(A.im, [A(), A(), A()])
    assert m





# ----------------
# StaticMethods.py
# ----------------

class A :
    cv = 0

    def __init__ (self) :
        A.cv    += 1
        self.iv  = 1

    def im (self) :
        pass

    @classmethod
    def cm (cls) :
        pass

    @staticmethod
    def sm () :
        # cv += 1 # UnboundLocalError: cannot access local variable 'cv' where it is not associated with a value
        A.cv += 1

        # im()    # NameError: name 'im' is not defined
        # A.im()  # TypeError: im() missing 1 required positional argument: 'self'

        # cm()    # NameError: name 'cm' is not defined
        A.cm()

def test1 () :
    assert A.cv == 0

    A.sm()
    assert A.cv == 1

def test2 () :
    x = A()
    assert A.cv == 2
    assert x.iv == 1

    A.sm()
    x.sm()           # misleading
    assert A.cv == 4
    assert x.iv == 1

class MyList :
    def __init__ (self, a) :
        self.a = a

    @staticmethod
    def from_size (s) :
        return MyList(s * [0])

    @staticmethod
    def from_size_value (s, v) :
        return MyList(s * [v])

def test3 () :
    x = MyList.from_size(2)
    assert isinstance(x, MyList)
    assert x.a == [0, 0]

def test4 () :
    x = MyList.from_size_value(3, 4)
    assert isinstance(x, MyList)
    assert x.a == [4, 4, 4]





# ---------------
# ClassMethods.py
# ---------------

class A :
    cv = 0

    def __init__ (self) :
        A.cv    += 1
        self.iv  = 1

    def im (self) :
        pass

    @classmethod
    def cm (cls) :  # cls might be bound to a child type
        # cv   += 1 # UnboundLocalError: cannot access local variable 'cv' where it is not associated with a value
        cls.cv += 1

        # im()      # NameError: name 'im' is not defined
        # cls.im()  # TypeError: im() missing 1 required positional argument: 'self'

        # sm()      # NameError: name 'sm' is not defined
        cls.sm()

    @staticmethod
    def sm () :
        pass

def test1 () :
    assert A.cv == 0

    A.cm()
    assert A.cv == 1

def test2 () :
    x = A()
    assert A.cv == 2
    assert x.iv == 1

    A.cm()
    x.cm()           # misleading
    assert A.cv == 4
    assert x.iv == 1

class MyList :
    def __init__ (self, a) :
        self.a = a

    @classmethod
    def from_size (cls, s) :
        return cls(s * [0])

    @classmethod
    def from_size_value (cls, s, v) :
        return cls(s * [v])

class MyListChild (MyList) :
    pass

def test3 () :
    x = MyList.from_size(2)
    assert isinstance(x, MyList)
    assert x.a == [0, 0]

def test4 () :
    x = MyList.from_size_value(2, 3)
    assert isinstance(x, MyList)
    assert x.a == [3, 3]

def test5 () :
    x = MyListChild.from_size(2)
    assert isinstance(x, MyListChild)
    assert x.a == [0, 0]

def test6 () :
    x = MyListChild.from_size_value(2, 3)
    assert isinstance(x, MyListChild)
    assert x.a == [3, 3]
