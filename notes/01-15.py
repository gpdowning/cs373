# -----------
# Wed, 15 Jan
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 19 Jan: Blog  #1
    Sun, 19 Jan: Paper #1: Syllabus

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 17 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
"""





# input

2

2 2 2 2 0
2 1 1 2 2 2 2 2 2 3 3 0
1 2 2 2 2 2 2 2 2 2 2 2 0 3
2 2 2 1 2 2 2 2 2 3 2 3 0 0
2 2 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 3 2 2 2 3 2 2 3 2 3 2 2 2 0 0 0 0 0 0 0 0

3 3 3 3 2
1 3 2 1 2 3 2 3 3 0 3 0
3 2 2 3 3 2 3 3 3 2 2 3 2 3
2 3 3 2 3 3 2 2 2 2 3 3 2 2
2 3 1 2 2 2 3 1 1 3 1 3 1 1 3 1 2 1 2 3 3 3 1 2 3 1 1 2 3 3 1 2 1 3 1 3 3 2 3 2 2 1

# output

B-
B-





"""
read, eval, print (REPL)
"""

"""
kernel (Grades.py)
run harness (run_Grades.py)
test harness (test_Grades.py)

hr_Grades.py (combine Grades.py and run_Grades.py, remove import)
"""





# ---------
# Grades.py
# ---------

def grades_eval (l_l_scores: list[list[int]]) -> str :
    assert l_l_scores
    return "B-"





# --------------
# test_Grades.py
# --------------

import unittest # main, TestCase

import Grades

class test_grades_eval (unittest.TestCase) :
    def test_0 (self) -> None :
        l_l_scores: list[list[int]] = [[0, 0, 0, 0, 0]]
        letter:     str             = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B-")

if __name__ == "__main__" : #pragma: no cover
    unittest.main()





# Java

class A {
    String f () {
        # this -> invoking object
        return "A.f"}}

A x = new A();
x.f();         # A.f; this -> x

A y = new A();
y.f();         # A.f; this -> y





# Python

class A :
    def f (self) :
        return "A.f"

x = A()
x.f()   # A.f; self -> x
A.f(x)  # A.f; self -> x





# -------------
# run_Grades.py
# -------------

import Grades

def grades_read () -> list[list[int]] :
    # read newline
    input()

    # read scores
    l_l_scores: list[list[int]] = [[int(s) for s in input().split()] for _ in range(5)]

    return l_l_scores

def grades_print (letter: str) -> None :
    print(letter)

def main () -> None :
    # read number of test cases
    input()

    # read, eval, print (REPL)
    try :
        while True :
            l_l_scores: list[list[int]] = grades_read()
            letter:     str             = Grades.grades_eval(l_l_scores)
            grades_print(letter)
    except EOFError :
        pass

if __name__ == "__main__" : #pragma: no cover
    main()





"""
Canvas
    assignment
    submit GitLab URL
    Academic Integrity Quiz
    don't do this until you're sure you've done everything

HackerRank
    contest
    submit as many times as you like
    4 tests, 3 tests to be able to resubmit

GitLab
    public code repo
        fork and clone

    public test repo
        between 200 and 300 tests
        merge request of your tests

    private code repo
        pylint, mypy, and black
        between 10 and 20 unit tests
        issue tracker (import issues) plus at least 5 more
        at least 5 commits
        pipeline
        README
            completion time (estimated and actual)
            git SHA
            Comments
                ChatGPT (how did you use it)

MOSS
"""
