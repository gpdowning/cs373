# -----------
# Wed,  5 Feb
# -----------

"""
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #4
    Sun, 9 Feb: Paper #4

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri,  7 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Mon, 10 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
"""





"""
last time
    unpacking
    Exercise #3: reduce()

this time
    Project #2: IDB1
"""





z = x + y

z = x.__add__(y)

z = operator.add(x, y)





"""
CATME
    create the teams
    22 teams
    11 at 10 am, 11 at 11 am
    each TA will mentor 5 or 6 teams
    we'll create 22 channels on a Slack server
    we'll create 22 groups on Canvas

    3 peer reviews at the end of 3 of the 4 phases
"""





"""
. social justice
. an underserved community
. civic engagement
. teach all of us about something we don't know
"""





"""
https://docs.google.com/document/d/10DtuoKwDkN4tDQuPH5H5LueNx8csHAbTOy7duvKVPUk/edit?tab=t.0#heading=h.e5gjtoi4qfmv

build a good set of issues in your issue tracker (at least 30 issues)
"""





"""
Phase I

1 page: splash page

1 page: an about page
    info about each member
    issues  per member and total
    commits per member and total
    tests   per member and total

3 pages: one per model
    you need at least 3 models (carefully choose the models, vet the data sources very well)
    models have to be interconnected (e.g., in IMDB actors are in movies)
    gather info from the Web
    at least ONE of those data sources has to be a RESTful API
    grid of cards
    each card describes an instance
    only need 3 cards
        each card has 5 attributes (be careful choosing the attributes that can later be filtered by or sorted by)

9 pages: one per instance
    a lot of text
    a lot of mutimedia
"""





"""
22 public repos
    issues
    commits
    code
    tests

each team will have two roles
    customer
        provide 5 user stories for every phase and negotiate them
        assess the website and api and the implementation of the user stories of your developer

    developer
        receive 5 user stories for every phase and negotiate them

4 circles of groups
    2 at 10 am, a group of 5 and a group of 6
    2 at 11 am, a group of 5 and a group of 6
"""





"""
RESTful APIs

1. GitLab API, to obtain issues and commits by the team members
2. your API
3. at least one data source
"""





"""
frontend
    rendering data
    get data from a RESTful

backend
    housing data with a DB
    provide a RESTful API
"""





"""
PechaKucha presentation in Phase IV
20 slides, 20 sec per slide, done live without reading
look it up
"""
